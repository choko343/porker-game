#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#define NSTRS_FROM_CLIENT 3
#define NSTRS_FROM_SERVER 2
#define MAX 65535
int sock=0;
char IP_str[MAX],Port_str[MAX];

//--------------------------------------------------------------------------//
// 強制終了時の関数
//
//
//--------------------------------------------------------------------------//

void Ctrl_c(){

	close(sock);

//	endwin();	//ncursesの終了

	exit(1);	//プログラムの終了
}

void Init_signal(){

	signal(SIGINT,Ctrl_c);
	signal(SIGTERM,Ctrl_c);
	signal(SIGQUIT,Ctrl_c);
	signal(SIGHUP,Ctrl_c);
}

void StartMessage(){
	char buf[MAX];
	int n;
	printf("Welcome to Poker Game\n");
	do{
		printf("Server Address >");
		fgets(buf,sizeof(buf),stdin);
		n = sscanf(buf,"%s",IP_str);
		if(n<1){
			sprintf(IP_str,"127.0.0.1");
			break;
		}
	}while(n != 1);
	do{
		printf("Port Number >");
		fgets(buf,sizeof(buf),stdin);
		n = sscanf(buf,"%s",Port_str);

	}while(n != 1);
	return;
}
//--------------------------------------------------------------------------//
// IPアドレスとポート番号を固定（デバッグ用）
//
//
//--------------------------------------------------------------------------//
void StartDebug(){
	char buf[MAX];
	int n;
	printf("Welcome to Poker Game\n");
	sprintf(IP_str,"127.0.0.1");
	do{
		printf("Port Number >");
		fgets(buf,sizeof(buf),stdin);
		n = sscanf(buf,"%s",Port_str);

	}while(n != 1);
	printf("IP = %s\nPort = %s\n",IP_str,Port_str);
	return;
}

//--------------------------------------------------------------------------//
// サーバーへ接続
//
//
//--------------------------------------------------------------------------//

void ConnectServer(void){

	struct sockaddr_in sain;
	struct hostent *hp;
	int len,size;

	//WorkClear();

	if(sock!=0){

	}
	else{

		if((sock=socket(AF_INET,SOCK_STREAM,0))==-1){
			//CommentDialog("Socket failed.");
			perror("Opening stream socket");
		}

		sain.sin_family=AF_INET;
		if((hp=gethostbyname(IP_str))==NULL){
			//CommentDialog("gethostbyname failed.");
			perror("Unknown host");
			close(sock);sock=0;
		}
		else{
			len=hp->h_length;
			bcopy((char *)hp->h_addr,(char *)&sain.sin_addr,len);
			sain.sin_port=htons(atoi(Port_str));

			size=sizeof(sain);
			if(connect(sock,(struct sockaddr *)&sain,size)==-1){
				//CommentDialog("Connect failed.");
				perror("Connecting socket stream");
				close(sock);sock=0;
			}
		}
	}
	//TouchWin();
	return;
}

void RecvMes(){
	char buf[1],mes[MAX],smes[MAX],data[MAX],cmd1[MAX],cmd2[MAX];
	char c;
	int i=0;
	/*FILE *fp;
	if ((fp = fopen("client.log", "a")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	// (3)エラーの場合は通常、異常終了する 
	}*/
	//printf("receiving\n");
	while(1){
		//printf("receiving\n");
		if(recv(sock,&c,1,0)==-1){
			close(sock);
			perror("Recv failed.");	
			exit(1);
		}
		if(c=='\0'){
			//printf("mes=[%s]",mes);
			mes[i]='\0';
			break;
		}else {
			//printf("c=%c\n",c);
			mes[i]=c;
			i++;
		}
	}
	//printf("receive finished\n");
	//fprintf(fp,"受信----------------------------------------\n%s\n\n",mes);
	sscanf(mes,"%[^,],%[^,],%[^,]",data,cmd1,cmd2);

	//printf("Command=[%s][%s][%s]\n",data,cmd1,cmd2);
	if(strcmp(cmd2,"PRINT")==0){
		printf("%s",data);
		fflush(stdout);
	}
	if(strcmp(cmd1,"SEND")==0){
		fgets(mes,sizeof(mes),stdin);
		sscanf(mes,"%s",smes);

		if(send(sock,smes,strlen(smes)+1,0)==-1){
			close(sock);
			perror("Send failed");
			exit(1);
		}
		/*if(strncmp(smes,"exit",4) == 0){
			Ctrl_c();
		}*/

	}
	if(strcmp(cmd1,"CHANGE")==0){
		fgets(mes,sizeof(mes),stdin);

		if(send(sock,mes,strlen(mes)+1,0)==-1){
			close(sock);
			perror("Send failed");
			exit(1);
		}

	}
	sprintf(smes,"A");
	if(strncmp(cmd1,"CHECK",5)==0){
		if(send(sock,smes,strlen(smes)+1,0)==-1){
			close(sock);
			perror("Send failed");
			exit(1);
		}
	}

	if(strncmp(cmd1,"QUIT",4)==0){
		Ctrl_c();

	}

	//fclose(fp);
	return;
}

int main(int argc,char *argv[]){
	char c;
	int flag = 0;
	char mes[MAX];
	char rmes[MAX];
	Init_signal();
	StartMessage();
	//StartDebug();						//デバッグ用
	ConnectServer();

	while(1){
		RecvMes();
		//printf("roop\n");
	}
	
	
}


