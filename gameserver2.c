#include <sys/types.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/ipc.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>


#define ROOMMAX 5
#define CARDMAX 53
#define MEMMAX 2											//１ルームのメンバー数
#define SIZE 65536
#define MAX 512
#define HELPMSG "commands,RECV,PRINT"
struct sharememory
{
	int master;
	int roomid;
	int gameid;
	int participants;
	int parentid;
	int deck[53];
	int deck_p;
	int fieldmoney;
	int gamecount;
	int beted;						//ベットしたかどうか
	int betmoney;
	int falled[MEMMAX + 1];			//降りたかどうか	
	int falledcount;
	
	int mycards[MEMMAX + 1][5];	//[member][card]
	int changecode[MEMMAX + 1];
	int readflag[MEMMAX + 1];
	int gamestate[MEMMAX + 1];	
	char rank[MEMMAX + 1][MAX];
	char action[MEMMAX + 1][MAX];
	int actionint[MEMMAX + 1];

	int money[MEMMAX + 1];

	char send_buf[MEMMAX + 1][MAX];
	char recv_buf[MEMMAX + 1][MAX];
	
	
};
int sock,newsock;
int len,len_c,len_r;
int sid;	//shared memory id
struct sharememory *data;
struct sockaddr_in sain,sain_c;

//--------------------------------------------------------------------------//
// デバッグ用関数
//
//
//--------------------------------------------------------------------------//

void DebugVar(int *var,int num){
	int i;
	FILE *fp;
	if((fp=fopen("log.txt","a"))==NULL){
        printf("Cannot open data.txt");
        exit(1);
	}
	fprintf(fp,"Debug\n");
	for(i=0;i<num;i++){
		fprintf(fp,"%d\n",var[i]);
	}
	fclose(fp);
	return;
}

//--------------------------------------------------------------------------//
// 強制終了時の関数
//
//
//--------------------------------------------------------------------------//

void Ctrl_c(){

	close(sock);
	close(newsock);
	printf("Socket closed\n");
	if(shmctl(sid,IPC_RMID,0)==-1){
		perror("shmctl failed.\n");
		exit(1);
	}
	printf("Shared memory closed\n");
	exit(1);	//プログラムの終了
}

void Init_signal(){

	signal(SIGINT,Ctrl_c);
	signal(SIGTERM,Ctrl_c);
	signal(SIGQUIT,Ctrl_c);
	signal(SIGHUP,Ctrl_c);
}

//--------------------------------------------------------------------------//
// 共有メモリ初期設定
//(ShmGetはいらないかも)
//
//--------------------------------------------------------------------------//

void ShmSetup(){

	if((sid=shmget(94,MAX*sizeof(struct sharememory),0666|IPC_CREAT))==-1){
		perror("shmget faild.\n");
		exit(1);
	}
	if((data=(struct sharememory *)shmat(sid,0,0))==(struct sharememory *)-1){
		perror("shmat faild.\n");
		exit(1);
	}
	return;
}

void InitShm(){
	int i,j;
	for(i=0;i<ROOMMAX;i++){
		data[i].master = 0;
		data[i].participants = 0;
		data[i].fieldmoney = 0;
		data[i].parentid = 1;
		data[i].beted = 0;
		data[i].gamecount = 1;
		for(j=0;j<MEMMAX+1;j++){
			data[i].readflag[j]=0;
			data[i].money[j]=10000;
		}
	}
	return;
}

/*void ShmGet(int *data){

	if((sid=shmget(94,MAX*sizeof(int),0666))==-1){
		perror("shmget faild.\n");
		exit(1);
	}
	printf("id=[%d]",sid);
	if((data=(int *)shmat(sid,0,0))==(int *)-1){
		perror("shmat faild.\n");
		exit(1);
	}
	printf("data=[%x]",data);
	return;
}*/

//--------------------------------------------------------------------------//
// TCPの初期設定
//
//
//--------------------------------------------------------------------------//

void ServerSetup(){

	if((sock=socket(AF_INET,SOCK_STREAM,0))==-1){
		perror("socket");
		exit(1);
	}
	sain.sin_family=AF_INET;
	sain.sin_addr.s_addr=INADDR_ANY;
	sain.sin_port=0;
	//sain.sin_port=htons(50000);							//デバッグ用--------------------------------------------	デバッグ用	

	if(bind(sock,(struct sockaddr *)&sain,sizeof(sain)) < 0){
		perror("bind");
		exit(1);
	}
	
	len = sizeof(sain);
	if(getsockname(sock,(struct sockaddr *)&sain,&len)==-1){
		perror("Getting sock name");
		exit(1);
	}

	printf("Server Port #%d\n",ntohs(sain.sin_port));

	if(listen(sock,5)==-1){
		perror("listen");
		exit(1);
	}
}

//--------------------------------------------------------------------------//
// メッセージの送信
//
//
//--------------------------------------------------------------------------//

void SendMes(char data[MAX],char cmd1[MAX],char cmd2[MAX]){
	char send_buf[MAX];
	sprintf(send_buf,"%s,%s,%s",data,cmd1,cmd2);
	printf("send_buf:%s",send_buf);
	send(newsock,send_buf,strlen(send_buf)+1,0);
	return;
}

void SendCmd(char data[MAX],char cmd1[MAX],char cmd2[MAX],char cmd3[MAX],char cmd4[MAX]){
	char send_buf[MAX];
	sprintf(send_buf,"%s,%s,%s,%s,%s",data,cmd1,cmd2,cmd3,cmd4);
	printf("send_buf:%s",send_buf);
	send(newsock,send_buf,strlen(send_buf)+1,0);
	return;
}

//--------------------------------------------------------------------------//
// メッセージの受信
//
//
//--------------------------------------------------------------------------//

void RecvMes(char cmd1[MAX]){
	char recv_buf[MAX];
	if((len_r=recv(newsock,recv_buf,SIZE,0))==-1){
		perror("Recv Fail");
		close(newsock);
		exit(1);
	}
		//printf("receive finished\n");
	recv_buf[len_r]='\0';
	printf("recv_buf : %s",recv_buf);
	sscanf(recv_buf,"%s",cmd1);
	return;
}

//--------------------------------------------------------------------------//
// メニュー
//
//
//--------------------------------------------------------------------------//

int Menu(void){
	char cmd1[MAX];
	int gamenum;
	while(1){
		SendMes("Porker Game\n2:Game\n3:Game\n>","SEND","PRINT");
		RecvMes(cmd1);
		if(strncmp(cmd1,"1",4)==0){
			gamenum = 1;
			break;
		}else if(strncmp(cmd1,"2",4)==0){
			gamenum = 2;
			break;
		}else if(strncmp(cmd1,"3",4)==0){
			gamenum = 3;
			break;
		}else{
			SendMes("Error\n","RECV","PRINT");
		}
	}
	return gamenum;
}

//--------------------------------------------------------------------------//
// カードのシャッフル
//
//
//--------------------------------------------------------------------------//

void Shuffle(int ary[],int size){
	int i,j,t;
	for(i=0;i<size;i++){
		j = rand()%size;
		t = ary[i];
		ary[i] = ary[j];
		ary[j] = t;
	}
	return;
}

//--------------------------------------------------------------------------//
// カードのコードを記号と数値に変更
//
//
//--------------------------------------------------------------------------//

void MapCard(int i,char buf[MAX]){
	if(i==0){
		sprintf(buf,"Joker");
	}else{
		switch(i/14){
			case 0:
				sprintf(buf,"%s","\x1b[31m♥\x1b[39m");
				break;
			case 1:
				sprintf(buf,"%s","\x1b[31m◆\x1b[39m");
				break;
			case 2:
				sprintf(buf,"%s","♠");
				break;
			case 3:
				sprintf(buf,"%s","♣");
				break;
		}
		sprintf(buf,"%s %2d ",buf,i%14);
	}

	return;
}

void MapGraph(int num,int mycards[5],char send_buf[MAX]){
	int i;
	char str[MAX];
	sprintf(send_buf,"");
	sprintf(str,"┌─────┐┌─────┐┌─────┐┌─────┐┌─────┐\n│     ││     ││     ││     ││     │\n");
	sprintf(send_buf,"%s%s",send_buf,str);
	for(i=0;i<num;i++){
		sprintf(str,"│");
		sprintf(send_buf,"%s%s",send_buf,str);
		MapCard(mycards[i],str);
		sprintf(send_buf,"%s%s",send_buf,str);
		sprintf(str,"│");
		sprintf(send_buf,"%s%s",send_buf,str);
	}
	sprintf(str,"\n│     ││     ││     ││     ││     │\n└─────┘└─────┘└─────┘└─────┘└─────┘\n");
	sprintf(send_buf,"%s%s",send_buf,str);
	
	return;

}

void MapAll(int num,int mycards[5],char send_buf[MAX]){
	int i;
	char str[MAX];
	for(i=0;i<num;i++){
		MapCard(mycards[i],str);
		sprintf(send_buf,"%s %s",send_buf,str);
	}
	
	return;
}

//--------------------------------------------------------------------------//
// 配列の中身のソート（降順）
//
//
//--------------------------------------------------------------------------//

void Sort(int ary[],int num){
	int i,j,tmp;
	//for(i=0;i<num;i++){printf("%d\n",ary[i]);}
	for(i=0;i<num;i++){
		for(j=i+1;j<num;j++){
			if(ary[i] < ary[j]){
				tmp = ary[j];
				ary[j] = ary[i];
				ary[i] = tmp;  
			}
		}
	}
	//for(i=0;i<num;i++){printf("e%d\n",ary[i]);}
	return;
}

//--------------------------------------------------------------------------//
// 配列の中身のソート（降順）%14
//
//
//--------------------------------------------------------------------------//

void SortNum(int ary[],int num){
	int i,j,tmp;
	int ary2[MAX];
	for(i=0;i<num;i++){
		ary2[i]=ary[i] % 14;
	}
	//for(i=0;i<num;i++){printf("%d\n",ary[i]);}
	for(i=0;i<num;i++){
		for(j=i+1;j<num;j++){
			if(ary2[i] < ary2[j]){
				tmp = ary2[j];
				ary2[j] = ary2[i];
				ary2[i] = tmp; 
				tmp = ary[j];
				ary[j] = ary[i];
				ary[i] = tmp; 
			}
		}
	}
	//for(i=0;i<num;i++){printf("e%d\n",ary[i]);}
	return;
}

//--------------------------------------------------------------------------//
// トランプのカードの初期化
//
//
//--------------------------------------------------------------------------//


void SetCard(int card[53]){
	int i,j=0;
	char buf[MAX];
	for(i=0;i<53;i++){
		if(j==14){j++;
		}else if(j==28){j++;
		}else if(j==42){j++;}
		card[i] = j;
		MapCard(card[i],buf);
		printf("Card[%d] %d = %s\n",card[i],j,buf);
		j++;
	}

	return;
}

//--------------------------------------------------------------------------//
// 役の確認
//
//
//--------------------------------------------------------------------------//

void CheckRank(int buf[MAX] ,char send_buf[MAX]){
	int i,j;
	for(i=0;i<5;i++){
		for(j=1+i;j<5;j++){
			if(buf[i]%14==buf[j]%14){
				sprintf(send_buf,"  1 Pair\n");
				return;
			}
		}
	}
	sprintf(send_buf,"  No Pair\n");
	return;
}

//--------------------------------------------------------------------------//
// カードを引く
//
//
//--------------------------------------------------------------------------//

int DrawCard(int card[],int deck[],int num,int pointer,int max){
	int i;
	for(i=0;i<num;i++){
		card[i] = deck[pointer];
		pointer++;
		if(pointer==max){pointer = 0;}
		//printf("Draw[%d]",card[i]);
	}
	Sort(card,5);
	return pointer;
}

int DrawOneCard(int card[],int deck[],int card_p,int deck_p,int max){
	int i;

	card[card_p] = deck[deck_p];
	deck_p++;
	if(deck_p==max){deck_p = 0;}
	
	return deck_p;
}

//--------------------------------------------------------------------------//
// カードの交換
//
//
//--------------------------------------------------------------------------//

int ChangeCard(int code,int card[],int deck[],int deck_p,int max){
	printf("\ncode[[[[%d]]]]\n",code);
	if(code % 2 == 0){
		deck_p=DrawOneCard(card,deck,0,deck_p,53);
	}
	if(code % 3 == 0){
		deck_p=DrawOneCard(card,deck,1,deck_p,53);
	}
	if(code % 5 == 0){
		deck_p=DrawOneCard(card,deck,2,deck_p,53);
	}
	if(code % 7 == 0){
		deck_p=DrawOneCard(card,deck,3,deck_p,53);
	}	
	if(code % 11 == 0){
		deck_p=DrawOneCard(card,deck,4,deck_p,53);
	}

	return deck_p;
}

//--------------------------------------------------------------------------//
// マスターの処理待ち関数
//
//
//--------------------------------------------------------------------------//

void WaitPlayer(int roomid,int state){
	int i;
	for(i=1;i<=data[roomid].participants;i++){
		while(data[roomid].gamestate[i] != state){sleep(1);}
	}
	return;
}

//--------------------------------------------------------------------------//
// BET処理関数
//
//
//--------------------------------------------------------------------------//

void RaiseMaster(int roomid, int raiser){
	int i,j,next;
	int participants = data[roomid].participants;

	printf("raiser=%d\n",raiser);

	data[roomid].betmoney = data[roomid].fieldmoney + 100;
	data[roomid].money[raiser] -= data[roomid].betmoney;
	data[roomid].fieldmoney += data[roomid].betmoney;

	for(i=1;i<=participants;i++){
		sprintf(data[roomid].send_buf[i],"Player%dがRaiseしました\n場のチップ:%d\n",raiser,data[roomid].fieldmoney);
		data[roomid].gamestate[i]=2;
		while(data[roomid].gamestate[i] != 0){sleep(1);}
	}

	next = raiser + 1;

	for(i=next;i<=participants;i++){

		if(data[roomid].falled[i] == 1){
			printf("降りている\n");
			continue;
		}

		data[roomid].gamestate[i] = 7;
		while(data[roomid].gamestate[i] != 0){sleep(1);}
		printf("kokoko");
		switch(data[roomid].actionint[i]){
			case 3:	//call
				data[roomid].money[i]-= data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 4:
				RaiseMaster(roomid,i);
				return;
				break;
			case 5:	//falled
				data[roomid].falled[i] = 1;
				data[roomid].falledcount++;
				printf("falledしたa\n");
				break;
		}
		for(j=1;j<=participants;j++){
			sprintf(data[roomid].send_buf[j],"Player%dが%sしました\n場のチップ:%d\n",i,data[roomid].action[i],data[roomid].fieldmoney);
			data[roomid].gamestate[j]=2;
			while(data[roomid].gamestate[j] != 0){sleep(1);}
		}

	}

	for(i=1;i<raiser;i++){
		if(data[roomid].falled[i] == 1){
			printf("降りている\n");
			continue;
		}

		data[roomid].gamestate[i] = 7;
		while(data[roomid].gamestate[i] != 0){sleep(1);}
		printf("koko");
		switch(data[roomid].actionint[i]){
			case 3:	//call
				data[roomid].money[i]-= data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 4:
				RaiseMaster(roomid,i);
				return;
				break;
			case 5:	//falled
				data[roomid].falled[i] = 1;
				data[roomid].falledcount++;
				printf("falledした\n");
				break;
		}
		for(j=1;j<=participants;j++){
			sprintf(data[roomid].send_buf[j],"Player%dが%sしました\n場のチップ:%d\n",i,data[roomid].action[i],data[roomid].fieldmoney);
			data[roomid].gamestate[j]=2;
			while(data[roomid].gamestate[j] != 0){sleep(1);}
		}

	}
	return;
	

}


void BetMaster(int roomid){
	int i,j,k;
	int raiseflag = 0;
	int participants = data[roomid].participants;
	int secondflag = 0;

	if(data[roomid].beted == 1){
		secondflag = 1;
		data[roomid].beted = 0;
	}

	for(i=1;i<=participants;i++){
		if(secondflag == 0){
			data[roomid].gamestate[i]=6;
		}else{
			data[roomid].gamestate[i]=8;
		}
		while(data[roomid].gamestate[i] != 0){sleep(1);}

		switch(data[roomid].actionint[i]){
			case 1://bet
				data[roomid].beted = 1;
				data[roomid].money[i]-=data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 2:	//check
				break;
			case 3:	//call
				data[roomid].money[i]-= data[roomid].fieldmoney;
				data[roomid].fieldmoney += data[roomid].fieldmoney;
				break;
			case 4:
				RaiseMaster(roomid,i);
				raiseflag = 1;
				break;
			case 5:	//falled
				data[roomid].falled[i] = 1;
				data[roomid].falledcount++;
				break;
		}
		if(raiseflag == 0){
			for(j=1;j<=participants;j++){
				sprintf(data[roomid].send_buf[j],"Player%dが%sしました\n場のチップ:%d\n",i,data[roomid].action[i],data[roomid].fieldmoney);
				data[roomid].gamestate[j]=2;
				while(data[roomid].gamestate[j] != 0){sleep(1);}
			}
		}

		if(participants - data[roomid].falledcount == 1){
			for(j=1;j<=participants;j++){
				sprintf(data[roomid].send_buf[j],"勝負できるプレイヤーがいなくなりました。\n");
				data[roomid].gamestate[j]=2;
				while(data[roomid].gamestate[j] != 0){sleep(1);}
				//チップの支払い
				if(data[roomid].falled[j] == 0){
					data[roomid].money[j] += data[roomid].fieldmoney;
					for(k=1;k<=participants;k++){
						sprintf(data[roomid].send_buf[k],"プレーヤー%dの勝利です。\n",j);
						data[roomid].gamestate[k]=2;
						while(data[roomid].gamestate[k] != 0){sleep(1);}
					}
				}
			}
			break;
		}


	}
}

void RaisePlayer(int roomid,int memberid,char *recv_buf){
	while(1){
		SendMes("Raise>","SEND","PRINT");
		RecvMes(recv_buf);
		sscanf(recv_buf,"%[^,]",data[roomid].action[memberid]);
		if(strcmp(data[roomid].action[memberid],"bet")==0){
			SendMes("できないよ\n","RECV","PRINT");
			continue;
		}else if(strcmp(data[roomid].action[memberid],"check")==0){
			SendMes("できないよ\n","RECV","PRINT");
			continue;
		}else if(strcmp(data[roomid].action[memberid],"call")==0){
			data[roomid].actionint[memberid] = 3;
			break;
		}else if(strcmp(data[roomid].action[memberid],"raise")==0){
			data[roomid].actionint[memberid] = 4;
			break;
		}else if(strcmp(data[roomid].action[memberid],"falled")==0){
			data[roomid].actionint[memberid] = 5;
			break;
		}else{
			data[roomid].actionint[memberid] = 0;
			SendMes("できんがな\n","RECV","PRINT");
		}
	}
	return;
}

void BetPlayer(int roomid,int memberid,char *recv_buf){
	char str[MAX];
	while(1){
		SendMes("BET>","SEND","PRINT");
		RecvMes(recv_buf);
		sscanf(recv_buf,"%[^,]",data[roomid].action[memberid]);
		if(strcmp(data[roomid].action[memberid],"bet")==0){
			data[roomid].actionint[memberid] = 1;
		}else if(strcmp(data[roomid].action[memberid],"check")==0){
			data[roomid].actionint[memberid] = 2;
		}else if(strcmp(data[roomid].action[memberid],"call")==0){
			data[roomid].actionint[memberid] = 3;
		}else if(strcmp(data[roomid].action[memberid],"raise")==0){
			data[roomid].actionint[memberid] = 4;
		}else if(strcmp(data[roomid].action[memberid],"falled")==0){
			data[roomid].actionint[memberid] = 5;
		}else{
			data[roomid].actionint[memberid] = 0;
		}
		if(data[roomid].beted == 0){
			if(data[roomid].actionint[memberid]==1 || data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes("できません\n","RECV","PRINT");
			}
		}else{
			if(data[roomid].actionint[memberid]==3 || data[roomid].actionint[memberid]==4|| data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes("できません\n","RECV","PRINT");
			}
		}
	}
	return;
}

void BetPlayer2(int roomid,int memberid,char *recv_buf){
	char str[MAX];
	while(1){
		SendMes("BET2>","SEND","PRINT");
		RecvMes(recv_buf);
		sscanf(recv_buf,"%[^,]",data[roomid].action[memberid]);
		if(strcmp(data[roomid].action[memberid],"bet")==0){
			data[roomid].actionint[memberid] = 1;
		}else if(strcmp(data[roomid].action[memberid],"check")==0){
			data[roomid].actionint[memberid] = 2;
		}else if(strcmp(data[roomid].action[memberid],"call")==0){
			data[roomid].actionint[memberid] = 3;
		}else if(strcmp(data[roomid].action[memberid],"raise")==0){
			data[roomid].actionint[memberid] = 4;
		}else if(strcmp(data[roomid].action[memberid],"falled")==0){
			data[roomid].actionint[memberid] = 5;
		}else{
			data[roomid].actionint[memberid] = 0;
		}
		if(data[roomid].beted==0){
			if(data[roomid].actionint[memberid]==1 || data[roomid].actionint[memberid]==2 || data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes("できません\n","RECV","PRINT");
			}
		}else{
			if(data[roomid].actionint[memberid]==3 || data[roomid].actionint[memberid]==4|| data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes("できません\n","RECV","PRINT");
			}
		}
	}
	return;
}
/*
void CheckWin(int roomid,int participants){
	int testwinner = 1;
	
	data[roomid].money[testwinner] += data[roomid].fieldmoney;
	data[roomid].fieldmoney = 0;
	sprintf(data[roomid].send_buf,"%dの勝ちです\n",testwinner);
	return;
}
*/
void DealCard(int roomid){
	int i;
	int participants = data[roomid].participants;
	char str[MAX];

	for(i=1;i<=participants;i++){
		sprintf(data[roomid].send_buf[i],"\033[2J\e[0;0H第%dゲームスタート\nあなたの所持チップは$%dです\n参加金$100を支払います。\n",data[roomid].gamecount,data[roomid].money[i]);
		data[roomid].fieldmoney += 100;
		data[roomid].money[i] -= 100;
		data[roomid].gamestate[i]=2;
		while(data[roomid].gamestate[i] != 0){sleep(1);}
	}

	for(i=1;i<=participants;i++){
		data[roomid].deck_p=DrawCard(data[roomid].mycards[i],data[roomid].deck,5,data[roomid].deck_p,CARDMAX);
		SortNum(data[roomid].mycards[i],5);
		CheckRank(data[roomid].mycards[i],data[roomid].rank[i]);
		MapGraph(5,data[roomid].mycards[i],data[roomid].send_buf[i]);
		sprintf(str,"%s",data[roomid].rank[i]);
		sprintf(data[roomid].send_buf[i],"%s%s",data[roomid].send_buf[i],str);
		data[roomid].gamestate[i]=2;
		while(data[roomid].gamestate[i] != 0){sleep(1);}

	}

	return;
}

void ChangeMaster(int roomid){
	int i;
	int participants = data[roomid].participants;
	char str[MAX];
	for(i=1;i<=participants;i++){
		if(data[roomid].falled[i] == 0){
			data[roomid].gamestate[i] = 4;
		}
	}

	WaitPlayer(roomid,0);

	for(i=1;i<=participants;i++){
		if(data[roomid].falled[i] == 0){				
			data[roomid].deck_p=ChangeCard(data[roomid].changecode[i],data[roomid].mycards[i],data[roomid].deck,data[roomid].deck_p,53);
			SortNum(data[roomid].mycards[i],5);
			CheckRank(data[roomid].mycards[i],data[roomid].rank[i]);
			MapGraph(5,data[roomid].mycards[i],data[roomid].send_buf[i]);
			sprintf(str,"%s",data[roomid].rank[i]);
			sprintf(data[roomid].send_buf[i],"%s%s",data[roomid].send_buf[i],str);
			data[roomid].gamestate[i]=2;
		}
	}
	WaitPlayer(roomid,0);
	return;
}

void OpenCard(int roomid){
	int i;
	int participants = data[roomid].participants;
	for(i=1;i<=participants;i++){
		if(data[roomid].falled[i] == 0){
			data[roomid].gamestate[i]=5;
		}
	}

}

void InitGame(roomid){
	int i;
	int participants = data[roomid].participants;
	data[roomid].beted = 0;
	data[roomid].fieldmoney = 0;
	data[roomid].falledcount = 0;
	data[roomid].betmoney = 100;
	for(i=1;i<=participants;i++){
		data[roomid].falled[i] = 0;
		
	}
	
	return;
}

//--------------------------------------------------------------------------//
// ポーカーゲームマスターの動作
//
//
//--------------------------------------------------------------------------//

void PokerMaster(int roomid){
	int i;
	int deck_p=0;
	int parentid = 1;

	while(data[roomid].participants<MEMMAX){sleep(1);}				//メンバーが揃うまで待機1

	SetCard(data[roomid].deck);

	while(1){
		InitGame(roomid);
		//Shuffle(data[roomid].deck,CARDMAX);
		
		//カードを配る
		DealCard(roomid);

		//第1BET

		BetMaster(roomid);
		if(data[roomid].participants - data[roomid].falledcount == 1){
			data[roomid].gamecount++;
			continue;
		}
		//カード交換
		ChangeMaster(roomid);
		
		//第2BET
		BetMaster(roomid);
		if(data[roomid].participants - data[roomid].falledcount == 1){
			data[roomid].gamecount++;
			continue;
		}
		//カード公開
		OpenCard(roomid);

		data[roomid].gamecount++;
	}

	return;
}

void PokerMember(int roomid){
	int i;
	int memberid;
	int code;
	char *s;
	char recv_buf[MAX],send_buf[MAX],str[MAX];

	data[roomid].participants++;
	memberid = data[roomid].participants;
	SendMes("Poker Start\n","RECV","PRINT");
	sprintf(send_buf,"\033[2J\e[0;0HあなたのIDは%dです。通信待機中\n",memberid);
	SendMes(send_buf,"RECV","PRINT");
	data[roomid].gamestate[memberid] = 0;
	while(1){
		while(data[roomid].gamestate[memberid] == 0){sleep(1);}
		switch(data[roomid].gamestate[memberid]){
			case 1:	
				RecvMes(data[roomid].recv_buf[memberid]);
				break;
			case 2:
				SendMes(data[roomid].send_buf[memberid],"RECV","PRINT");
				break;
			case 3:
				SendMes(data[roomid].send_buf[memberid],"SEND","PRINT");
				RecvMes(data[roomid].recv_buf[memberid]);
				break;
			case 4:
				SendMes("交換>","CHANGE","PRINT");
				RecvMes(recv_buf);
				data[roomid].changecode[memberid] = atoi(recv_buf);
				code = 1;
				s = strtok(recv_buf," ");
				while(s != NULL){
					switch(atoi(s)){
						case 1: code = code * 2; break;
						case 2: code = code * 3; break;
						case 3: code = code * 5; break;
						case 4: code = code * 7; break;
						case 5: code = code * 9; break;
						default : code = code * atoi(s); break;
					}
					s = strtok(NULL," ");
				}

				SendMes("通信待機中...\n","RECV","PRINT");
				break;
			case 5:		//手札公開
				sprintf(send_buf,"\033[2J\e[0;0H手札オープン\nYou:\n");
				SendMes(send_buf,"RECV","PRINT");
				MapGraph(5,data[roomid].mycards[memberid],send_buf);
				SendMes(send_buf,"RECV","PRINT");
				CheckRank(data[roomid].mycards[memberid],send_buf);
				SendMes(send_buf,"RECV","PRINT");
				for(i=1;i<=data[roomid].participants;i++){
					if(i!=memberid){
						sprintf(send_buf,"Member%d:\n",i);
						SendMes(send_buf,"RECV","PRINT");
						MapGraph(5,data[roomid].mycards[i],send_buf);
						SendMes(send_buf,"RECV","PRINT");
						CheckRank(data[roomid].mycards[i],send_buf);
						SendMes(send_buf,"RECV","PRINT");
					}
				}
				break;
			case 6:
				BetPlayer(roomid,memberid,recv_buf);
				break;
			case 7:
				RaisePlayer(roomid,memberid,recv_buf);
				break;
			case 8:
				BetPlayer2(roomid,memberid,recv_buf);
				break;
			default:
				break;
		}
		data[roomid].gamestate[memberid] = 0;
	}
}

//--------------------------------------------------------------------------//
// ポーカーゲーム本体
//
//
//--------------------------------------------------------------------------//

void PokerGame(void){
	int i,j,pid;
	int x;
	int deck_num=0;
	int deck[53];
	int mycards[5];
	int myrank;
	int roomid,master=0;
	int memberid;
	char str[MAX];
	char send_buf[MAX];
	char recv_buf[MAX];

	RecvMes(recv_buf);

	sprintf(send_buf,"部屋を選んでね。");
	SendCmd(send_buf,"RECV","WORK","1","6");
	sprintf(str,"");
	while(1){
		sprintf(send_buf,"%s%s",send_buf,str);
		for(i=0;i<ROOMMAX;i++){
			sprintf(str,"");
			sprintf(send_buf,"Room %d : %d / %d",i,data[i].participants,MEMMAX);
			if(data[i].participants == MEMMAX){
				sprintf(send_buf,"%s \x1b[31m満員\x1b[39m",send_buf);
			}
			sprintf(str,"%d",3 + i * 2);
			SendCmd(send_buf,"RECV","WORK",str,"6");
		}

		SendCmd(send_buf,"SEND","MENUCURSOR","","");
		RecvMes(recv_buf);
		roomid = atoi(recv_buf);

		if(roomid>ROOMMAX){
			sprintf(str,"\033[2J\e[0;0H部屋[%d]は存在しません。\n",roomid);
			break;
		}


		if(data[roomid].participants < MEMMAX){
			break;
		}else{
			sprintf(str,"\033[2J\e[0;0H部屋[%d]は満員です。\n",roomid);
		}
	}

	pid=fork();
	if(pid!=0){
		close(newsock);
		if(data[roomid].master == 0){
			data[roomid].master = 1;
			master = 1;
		}else{
			exit(1);
		}
	}

	if(master==1){
		PokerMaster(roomid);
	}else{
		PokerMember(roomid);
		return;
	}
}

//--------------------------------------------------------------------------//
// メイン関数
//
//
//--------------------------------------------------------------------------//

int main(void){
	//FILE *fp;
	int cn;
	int pid;
	
	int gamenum;
	srand((unsigned int)time(NULL));
	
	Init_signal();
	ShmSetup();
	InitShm();
	ServerSetup();

	

	while(1){
		len_c = sizeof(sain_c);
		newsock = accept(sock,(struct sockaddr*)&sain_c,&len_c);
		if(newsock==-1){
			perror("accept");
			exit(1);
		}
		
		//child process generate
		pid=fork();
		if(pid!=0){
			close(newsock);
			continue;
		}

		PokerGame();

		printf("Connection closed.\n");
		close(newsock);
		break;
	}
	return 0;
}


