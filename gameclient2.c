//--------------------------------------------------------------------------//
// コンパイルコマンド
// gcc -o gameclient2 gameclient2.c -lncursesw
//
//--------------------------------------------------------------------------//

#include <ncurses.h>
#include <locale.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <netdb.h>
#define MAX 512
WINDOW *BaseWin=NULL,*MenuWin=NULL;
WINDOW *TitleWin=NULL,*WorkWin=NULL;
WINDOW *StateWin=NULL;
int sock=0;
char IP_str[MAX],Port_str[MAX];
void Init_win(void)
{
	BaseWin=initscr();

	TitleWin=newwin(3,COLS,0,0);
	box(TitleWin,0,0);
	mvwprintw(TitleWin,1,COLS/2-4,"Poker Game");
	
	MenuWin=newwin(LINES-3,12,3,0);
	box(MenuWin,0,0);

	WorkWin=newwin(LINES-3,COLS-24,3,24);
	box(WorkWin,0,0);

	StateWin=newwin(LINES-3,12,3,12);
	box(StateWin,0,0);

	wrefresh(TitleWin);wrefresh(MenuWin);wrefresh(WorkWin);wrefresh(StateWin);
}

void Ctrl_c(){

	close(sock);

	endwin();	//ncursesの終了

	exit(1);	//プログラムの終了
}

void Init_signal(){

	signal(SIGINT,Ctrl_c);
	signal(SIGTERM,Ctrl_c);
	signal(SIGQUIT,Ctrl_c);
	signal(SIGHUP,Ctrl_c);
}

void Init_menu(void){

	mvwprintw(MenuWin,2,3,"接続");
	mvwprintw(MenuWin,4,3,"部屋選択");
	mvwprintw(MenuWin,6,3,"切断");
	mvwprintw(MenuWin,8,3,"機能1");
	mvwprintw(MenuWin,10,3,"機能2");
	mvwprintw(MenuWin,13,3,"End");
	mvwprintw(MenuWin,2,2,">");
	wmove(MenuWin,2,2);

	wrefresh(MenuWin);
}

void TouchWin(void){

	touchwin(BaseWin);wrefresh(BaseWin);
	touchwin(TitleWin);wrefresh(TitleWin);
	touchwin(MenuWin);wrefresh(MenuWin);
	touchwin(WorkWin);wrefresh(WorkWin);
	touchwin(StateWin);wrefresh(StateWin);
}

void CommentDialog(char *str){

	WINDOW *comment_win;
	int c,len,lines;

	len=strlen(str); lines=len/(COLS-9)+6;

	comment_win=newwin(lines,COLS-17,(LINES-lines)/2,14);
	box(comment_win,0,0);
	mvwprintw(comment_win,0,6,"Message!!");
	mvwprintw(comment_win,2,2,str);
	wrefresh(comment_win);

	noecho();
	keypad(comment_win,TRUE);
	wgetch(comment_win);

	delwin(comment_win); TouchWin();
}







void WorkClear(){

	wclear(WorkWin);
	box(WorkWin,0,0);
	wrefresh(WorkWin);
}



void ConnectServer(void){

	struct sockaddr_in sain;
	struct hostent *hp;
	int len,size;

	WorkClear();

	if(sock!=0){
		mvwprintw(WorkWin,2,6,"Connection Information");
		mvwprintw(WorkWin,8,4,"Server Address :");
		mvwprintw(WorkWin,12,4,"Connection Port: ");
		mvwprintw(WorkWin,8,20,IP_str);
		mvwprintw(WorkWin,12,16,Port_str);
	}
	else{
		mvwprintw(WorkWin,2,6,"接続情報:");
		mvwprintw(WorkWin,8,4,"サーバアドレス:");
		mvwprintw(WorkWin,12,4,"ポート番号:");

		echo();
		wmove(WorkWin,8,20); wgetstr(WorkWin,IP_str);
		wmove(WorkWin,12,16); wgetstr(WorkWin,Port_str);
		noecho();

		if((sock=socket(AF_INET,SOCK_STREAM,0))==-1){
			CommentDialog("Socket failed.");
		}

		sain.sin_family=AF_INET;
		if((hp=gethostbyname(IP_str))==NULL){
			CommentDialog("gethostbyname failed.");
			close(sock);sock=0;
		}
		else{
			len=hp->h_length;
			bcopy((char *)hp->h_addr,(char *)&sain.sin_addr,len);
			sain.sin_port=htons(atoi(Port_str));

			size=sizeof(sain);
			if(connect(sock,(struct sockaddr *)&sain,size)==-1){
				CommentDialog("Connect failed.");
				close(sock);sock=0;
			}
		}
	}
	TouchWin();
}

void RecvMes(){
	char buf[1],mes[MAX],smes[MAX],data[MAX],cmd1[MAX],cmd2[MAX],cmd3[MAX],cmd4[MAX];
	char str[MAX];
	char c;
	int x,y;
	char *s;
	int i=2;
	noecho();
	cbreak();
	keypad(WorkWin,TRUE);
	
	while(1){
		//printf("receiving\n");
		if(recv(sock,&c,1,0)==-1){
			close(sock);
			perror("Recv failed.");	
			exit(1);
		}
		if(c=='\0'){
			//printf("mes=[%s]",mes);
			mes[i]='\0';
			break;
		}else {
			//printf("c=%c\n",c);
			mes[i]=c;
			i++;
		}
	}

	beep();
	//printf("receive finished\n");
	sscanf(mes,"%[^,],%[^,],%[^,],%[^,],%[^,]",data,cmd1,cmd2,cmd3,cmd4);
	//printf("Command=[%s][%s][%s,%[^,]]\n",data,cmd1,cmd2);
	
	
	if(strcmp(cmd2,"PRINT")==0){
		printf("%s",data);
	}else if(strncmp(cmd2,"WORK",4)==0){
		x = atoi(cmd3);
		y = atoi(cmd4);
		//mvwprintw(WorkWin,10,1,"%s",data);
		s = strtok(data,"\n");
		while(s != NULL){
			sprintf(str,"%s",s);
			mvwprintw(WorkWin,x,y,"%s",str);
			s = strtok(NULL,"\n");
			x++;
			//wrefresh(WorkWin);
		}
	}/*else if(strcmp(cmd2,"MENUCURSOR")){
		while(1){
			c=wgetch(WorkWin);
			switch(c){
				case 10:	//enter
					//DoCommand(i);
					wmove(WorkWin,i,2);
					wrefresh(WorkWin);
					break;
				case 3:		//up key
					if(i>2){
						mvwprintw(WorkWin,i,2," ");
						mvwprintw(WorkWin,i-2,2,">");
						wmove(WorkWin,i-2,2);
						wrefresh(WorkWin);
						i-=2;
					}
					else{
						mvwprintw(WorkWin,2,2," ");
						mvwprintw(WorkWin,12,2,">");
						wmove(WorkWin,12,2);
						wrefresh(WorkWin);
						i=12;
					}
					break;
				case 2:		//down key
					if(i<12){
						mvwprintw(WorkWin,i,2," ");
						mvwprintw(WorkWin,i+2,2,">");
						wmove(WorkWin,i+2,2);
						wrefresh(WorkWin);
						i+=2;
					}
					else{
						mvwprintw(WorkWin,12,2," ");
						mvwprintw(WorkWin,2,2,">");
						wmove(WorkWin,2,2);
						wrefresh(WorkWin);
						i=2;
					}
					break;
				default :
					break;
		}
	}*/




	
	/*if(strcmp(cmd1,"SEND")==0){
		fgets(mes,sizeof(mes),stdin);
		sscanf(mes,"%s",smes);
		if(send(sock,smes,strlen(smes)+1,0)==-1){
			close(sock);
			perror("Send failed");
			exit(1);
		}

	}*/
	TouchWin();
	return;
}

void Game(void){
	char mes[MAX];

	WorkClear();
	sprintf(mes,"");
	if(send(sock,mes,strlen(mes)+1,0)==-1){
			close(sock);
			perror("Send failed");
			exit(1);
		}
	while(1){
		RecvMes();
	}

}



void Search(void){

	char search_str[MAX],send_buf[MAX],recv_buf[MAX];
	int rn;

	WorkClear();

	if(sock==0){
		CommentDialog("not connected");
	}
	else{
		mvwprintw(WorkWin,2,6,"Search Infomation");
		mvwprintw(WorkWin,8,4,"ZipCode: ");
		mvwprintw(WorkWin,12,4,"Search Result: ");

		echo();
		wmove(WorkWin,8,16); wgetstr(WorkWin,search_str);
		noecho();

		sprintf(send_buf,"getaddress %s",search_str);
		if(send(sock,send_buf,strlen(send_buf)+1,0)==-1){
			CommentDialog("Send failed.");
		}
		else{
			if((rn=recv(sock,recv_buf,MAX,0))==-1){
				CommentDialog("Recv faied.");
			}
			else{
				recv_buf[rn]='\0';
				mvwprintw(WorkWin,12,16,recv_buf);
			}
		}
	}
	TouchWin();
}



void CloseServer(void){

	WorkClear();

	if(sock!=0){
		close(sock);
		sock=0;
	}
	else{
		CommentDialog("Disconnected");
	}
}


void DoCommand(int i){

	char c,str[MAX];

	WorkClear();

	switch(i){
		case 2:
			ConnectServer();
			break;
		case 4:
			Game();
			break;
		case 6:
			CloseServer();
			break;
		case 8:
			//コマンド
			break;
		case 10:
			//コマンド
			break;
		case 12:
			sprintf(str,"Do you exit? [Y]es or [N]o :");
			mvwprintw(WorkWin,(LINES-4)/2,4,str);
			wrefresh(WorkWin);

			keypad(WorkWin,TRUE);
			c=wgetch(WorkWin);
			if(c==10) c='Y';
			switch(c){
				case 'y':
					endwin(); exit(1);
				case 'Y':
					endwin(); exit(1);
				default:
					break;
			}
			break;
		default:
			break;
	}
}




int main(void){
	
	int i=2;
	char c;
	setlocale(LC_ALL,"");								//文字化け対応
	Init_win();
	Init_signal();
	Init_menu();
	
	noecho();
	cbreak();
	keypad(MenuWin,TRUE);

	while(1){
		c=wgetch(MenuWin);
		switch(c){
			case 10:	//enter
				DoCommand(i);
				wmove(MenuWin,i,2);
				wrefresh(MenuWin);
				break;
			case 3:		//up key
				if(i>2){
					mvwprintw(MenuWin,i,2," ");
					mvwprintw(MenuWin,i-2,2,">");
					wmove(MenuWin,i-2,2);
					wrefresh(MenuWin);
					i-=2;
				}
				else{
					mvwprintw(MenuWin,2,2," ");
					mvwprintw(MenuWin,12,2,">");
					wmove(MenuWin,12,2);
					wrefresh(MenuWin);
					i=12;
				}
				break;
			case 2:		//down key
				if(i<12){
					mvwprintw(MenuWin,i,2," ");
					mvwprintw(MenuWin,i+2,2,">");
					wmove(MenuWin,i+2,2);
					wrefresh(MenuWin);
					i+=2;
				}
				else{
					mvwprintw(MenuWin,12,2," ");
					mvwprintw(MenuWin,2,2,">");
					wmove(MenuWin,2,2);
					wrefresh(MenuWin);
					i=2;
				}
				break;
			default :
				break;
		}
	}
	close(sock); endwin(); return 0;
}

