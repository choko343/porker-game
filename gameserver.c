#include <sys/types.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/ipc.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>


#define ROOMMAX 10
#define CARDMAX 53
#define MEMMAX 10											//１ルームのメンバー数
#define SIZE 65536
#define MAX 512
#define HELPMSG "commands,RECV,PRINT"
#define MSGWIN_X 2
#define MSGWIN_Y 21
#define CONSOLE_X 1
#define CONSOLE_Y 16


/* 評価値用 */
#define BASE 10000000000
#define S0 100000000
#define S1 1000000
#define S2 10000
#define S3 100
#define S4 1

struct sharememory
{
	int master;
	int roomid;
	int gameid;
	int participants;
	int parentid;
	int deck[53];
	int deck_p;
	int fieldmoney;
	int gamecount;
	int beted;						//ベットしたかどうか
	int betmoney;
	int falled[MEMMAX + 1];			//降りたかどうか	
	int falledcount;
	
	int participantsmax[10];

	int mycards[MEMMAX + 1][5];	//[member][card]
	int changecode[MEMMAX + 1];
	int gamestate[MEMMAX + 1];	
	unsigned long rankvalue[MEMMAX + 1];
	char rank[MEMMAX + 1][MAX];
	char action[MEMMAX + 1][MAX];
	int actionint[MEMMAX + 1];
	int changenum[MEMMAX + 1];
	int money[MEMMAX + 1];

	char send_buf[MEMMAX + 1][MAX];
	char recv_buf[MEMMAX + 1][MAX];
	
	
};
int sock,newsock;
int len,len_c,len_r;
int sid;	//shared memory id
struct sharememory *data;
struct sockaddr_in sain,sain_c;
unsigned long points;

int roomnum[11]={0,1,1,1,1,2,2,2,3,3,3};


//--------------------------------------------------------------------------//
// デバッグ用関数
//
//
//--------------------------------------------------------------------------//

void DebugVar(int *var,int num){
	int i;
	FILE *fp;
	if((fp=fopen("log.txt","a"))==NULL){
        printf("Cannot open data.txt");
        exit(1);
	}
	fprintf(fp,"Debug\n");
	for(i=0;i<num;i++){
		fprintf(fp,"%d\n",var[i]);
	}
	fclose(fp);
	return;
}

//--------------------------------------------------------------------------//
// 強制終了時の関数
//
//
//--------------------------------------------------------------------------//

void Ctrl_c(){

	close(sock);
	close(newsock);
	printf("Socket closed\n");
	if(shmctl(sid,IPC_RMID,0)==-1){
		perror("shmctl failed.\n");
		exit(1);
	}
	printf("Shared memory closed\n");
	exit(1);	//プログラムの終了
}

void Init_signal(){

	signal(SIGINT,Ctrl_c);
	signal(SIGTERM,Ctrl_c);
	signal(SIGQUIT,Ctrl_c);
	signal(SIGHUP,Ctrl_c);
}

//--------------------------------------------------------------------------//
// 共有メモリ初期設定
//(ShmGetはいらないかも)
//
//--------------------------------------------------------------------------//

void ShmSetup(){

	if((sid=shmget(94,MAX*sizeof(struct sharememory),0666|IPC_CREAT))==-1){
		perror("shmget faild.\n");
		exit(1);
	}
	if((data=(struct sharememory *)shmat(sid,0,0))==(struct sharememory *)-1){
		perror("shmat faild.\n");
		exit(1);
	}
	return;
}

void InitShm(){
	int i,j;
	for(i=0;i<ROOMMAX;i++){
		data[i].master = 0;
		data[i].participants = 0;
		data[i].fieldmoney = 0;
		data[i].parentid = 1;
		data[i].beted = 0;
		data[i].gamecount = 1;
		for(j=0;j<MEMMAX+1;j++){
			data[i].money[j]=10000;
		}
	}
	return;
}

/*void ShmGet(int *data){

	if((sid=shmget(94,MAX*sizeof(int),0666))==-1){
		perror("shmget faild.\n");
		exit(1);
	}
	printf("id=[%d]",sid);
	if((data=(int *)shmat(sid,0,0))==(int *)-1){
		perror("shmat faild.\n");
		exit(1);
	}
	printf("data=[%x]",data);
	return;
}*/

//--------------------------------------------------------------------------//
// TCPの初期設定
//
//
//--------------------------------------------------------------------------//

void ServerSetup(){

	if((sock=socket(AF_INET,SOCK_STREAM,0))==-1){
		perror("socket");
		exit(1);
	}
	sain.sin_family=AF_INET;
	sain.sin_addr.s_addr=INADDR_ANY;
	sain.sin_port=0;
	//sain.sin_port=htons(50000);							//デバッグ用--------------------------------------------	デバッグ用	

	if(bind(sock,(struct sockaddr *)&sain,sizeof(sain)) < 0){
		perror("bind");
		exit(1);
	}
	
	len = sizeof(sain);
	if(getsockname(sock,(struct sockaddr *)&sain,&len)==-1){
		perror("Getting sock name");
		exit(1);
	}

	printf("Server Port #%d\n",ntohs(sain.sin_port));

	if(listen(sock,5)==-1){
		perror("listen");
		exit(1);
	}
}

//--------------------------------------------------------------------------//
// メッセージの送信
//
//
//--------------------------------------------------------------------------//

void SendMes(int roomid, char mes[MAX],char cmd1[MAX],char cmd2[MAX]){
	char send_buf[MAX];
	int n;
	sprintf(send_buf,"%s,%s,%s",mes,cmd1,cmd2);
	//printf("send_buf:%s",send_buf);
	if((n = send(newsock,send_buf,strlen(send_buf)+1,0))==-1){
		data[roomid].participants = 0;
		data[roomid].master = 0;
		Ctrl_c();
	}

	return;
}

//--------------------------------------------------------------------------//
// メッセージの受信
//
//
//--------------------------------------------------------------------------//

void RecvMes(int roomid,char cmd1[MAX]){
	char recv_buf[MAX];
	int j;
	if((len_r=recv(newsock,recv_buf,SIZE,0))==-1){
		perror("Recv Fail");
		close(newsock);
		exit(1);
	}
	if(len_r == 0){
		data[roomid].participants = 0;
		data[roomid].master = 0;
		data[roomid].fieldmoney = 0;
		data[roomid].parentid = 1;
		data[roomid].beted = 0;
		data[roomid].gamecount = 1;
		for(j=0;j<MEMMAX+1;j++){
			data[roomid].money[j]=10000;
		}
		Ctrl_c();
	}

	if(strncmp(recv_buf,"exit",4)==0){
		data[roomid].participants = 0;
		data[roomid].master = 0;
		data[roomid].fieldmoney = 0;
		data[roomid].parentid = 1;
		data[roomid].beted = 0;
		data[roomid].gamecount = 1;
		for(j=0;j<MEMMAX+1;j++){
			data[roomid].money[j]=10000;
		}
	}
	recv_buf[len_r]='\0';

	printf("recv_buf : %s",recv_buf);
	printf("\n");
	sscanf(recv_buf,"%s",cmd1);
	return;
}

void RecvChange(int roomid,char cmd1[MAX]){
	char recv_buf[MAX];
	int j;
	if((len_r=recv(newsock,recv_buf,SIZE,0))==-1){
		perror("Recv Fail");
		close(newsock);
		exit(1);
	}
	if(len_r == 0){
		data[roomid].participants = 0;
		data[roomid].master = 0;
		data[roomid].fieldmoney = 0;
		data[roomid].parentid = 1;
		data[roomid].beted = 0;
		data[roomid].gamecount = 1;
		for(j=0;j<MEMMAX+1;j++){
			data[roomid].money[j]=10000;
		}
		Ctrl_c();
	}

	if(strncmp(recv_buf,"exit",4)==0){
		data[roomid].participants = 0;
		data[roomid].master = 0;
		data[roomid].fieldmoney = 0;
		data[roomid].parentid = 1;
		data[roomid].beted = 0;
		data[roomid].gamecount = 1;
		for(j=0;j<MEMMAX+1;j++){
			data[roomid].money[j]=10000;
		}
	}

	recv_buf[len_r]='\0';
	printf("recv_buf : %s",recv_buf);
	strcpy(cmd1,recv_buf);
	return;
}

//--------------------------------------------------------------------------//
// メニュー
//
//
//--------------------------------------------------------------------------//

int Menu(void){
	char cmd1[MAX];
	int gamenum;
	while(1){
		SendMes(0,"\033[2J\e[0;0H1:Porker Game\n2:Game\n3:Game\n>","SEND","PRINT");
		RecvMes(0,cmd1);
		if(strncmp(cmd1,"1",4)==0){
			gamenum = 1;
			break;
		}else if(strncmp(cmd1,"2",4)==0){
			gamenum = 2;
			break;
		}else if(strncmp(cmd1,"3",4)==0){
			gamenum = 3;
			break;
		}else{
			SendMes(0,"Error\n","RECV","PRINT");
		}
	}
	return gamenum;
}

//--------------------------------------------------------------------------//
// カードのシャッフル
//
//
//--------------------------------------------------------------------------//

void Shuffle(int ary[],int size){
	int i,j,t;
	for(i=0;i<size;i++){
		j = rand()%size;
		t = ary[i];
		ary[i] = ary[j];
		ary[j] = t;
	}
	return;
}

//--------------------------------------------------------------------------//
// カードのコードを記号と数値に変更
//
//
//--------------------------------------------------------------------------//

void MapCard(int i,char buf[MAX]){
	if(i==0){
		sprintf(buf,"Joker");
	}else{
		switch(i/14){
			case 0:
				sprintf(buf,"%s","\x1b[31m♥\x1b[39m");
				break;
			case 1:
				sprintf(buf,"%s","\x1b[31m◆\x1b[39m");
				break;
			case 2:
				sprintf(buf,"%s","♠");
				break;
			case 3:
				sprintf(buf,"%s","♣");
				break;
		}
		sprintf(buf,"%s %2d ",buf,i%14);
	}
	//printf("MAPCARD%d = %s\n",i,buf);
	return;
}

void MapGraph(int num,int mycards[5],char send_buf[MAX]){
	int i;
	char str[MAX];
	

	sprintf(send_buf,"");
	sprintf(str,"┌─────┐┌─────┐┌─────┐┌─────┐┌─────┐\n│     ││     ││     ││     ││     │\n");
	sprintf(send_buf,"%s%s",send_buf,str);
	for(i=0;i<num;i++){
		sprintf(str,"│");
		sprintf(send_buf,"%s%s",send_buf,str);
		MapCard(mycards[i],str);
		sprintf(send_buf,"%s%s",send_buf,str);
		sprintf(str,"│");
		sprintf(send_buf,"%s%s",send_buf,str);
	}
	sprintf(str,"\n│     ││     ││     ││     ││     │\n└─────┘└─────┘└─────┘└─────┘└─────┘\n");
	sprintf(send_buf,"%s%s",send_buf,str);
	
	return;

}

void MapAll(int num,int mycards[5],char send_buf[MAX]){
	int i;
	char str[MAX];
	for(i=0;i<num;i++){
		MapCard(mycards[i],str);
		sprintf(send_buf,"%s %s",send_buf,str);
	}
	
	return;
}

//--------------------------------------------------------------------------//
// 配列の中身のソート（降順）
//
//
//--------------------------------------------------------------------------//

void Sort(int ary[],int num){
	int i,j,tmp;
	//for(i=0;i<num;i++){printf("%d\n",ary[i]);}
	for(i=0;i<num;i++){
		for(j=i+1;j<num;j++){
			if(ary[i] < ary[j]){
				tmp = ary[j];
				ary[j] = ary[i];
				ary[i] = tmp;  
			}
		}
	}
	//for(i=0;i<num;i++){printf("e%d\n",ary[i]);}
	return;
}

//--------------------------------------------------------------------------//
// 配列の中身のソート（降順）%14
//
//
//--------------------------------------------------------------------------//

void SortNum(int ary[],int num){
	int i,j,tmp;
	int ary2[MAX];
	for(i=0;i<num;i++){
		ary2[i]=ary[i] % 14;
	}
	//for(i=0;i<num;i++){printf("%d\n",ary[i]);}
	for(i=0;i<num;i++){
		for(j=i+1;j<num;j++){
			if(ary2[i] < ary2[j]){
				tmp = ary2[j];
				ary2[j] = ary2[i];
				ary2[i] = tmp; 
				tmp = ary[j];
				ary[j] = ary[i];
				ary[i] = tmp; 
			}
		}
	}
	//for(i=0;i<num;i++){printf("e%d\n",ary[i]);}
	return;
}


//--------------------------------------------------------------------------//
// カードの順番を、表示用およびスコア算出用にAを14扱いでソートする関数 
// ただし54321のストレートの場合のみソートしない(Aを1として扱うため)
//
//--------------------------------------------------------------------------//

void SortShow(int ary[],int num, int rank,int jid){
	int i,j,tmp;
	int ary2[MAX];

	if(rank == 4 && (ary[4] % 14 == 1)){
		return;	/* 54321 の場合のみ何もせず終了 */
	}

	else {
		// A = 1,15,29,43 Aを一時的に14nに
		for(i=0; i<5; i++){
			if(ary[i] == 1) ary[i] = 14;
			if(ary[i] == 15) ary[i] = 28;
			if(ary[i] == 29) ary[i] = 42;
			if(ary[i] == 43) ary[i] = 56;
//			printf("ary[%d]=%d ",i,ary[i]);	// debug
		}

		for(i=0;i<num;i++){
			ary2[i] = (ary[i]%14)-1;
			if(ary2[i] == -1){
				ary2[i] = 13;
			}
		}

		for(i=0;i<num;i++){
			for(j=i+1;j<num;j++){
				if(ary2[i] < ary2[j]){
					tmp = ary2[j];
					ary2[j] = ary2[i];
					ary2[i] = tmp; 
					tmp = ary[j];
					ary[j] = ary[i];
					ary[i] = tmp; 
				}
			}
		}

		// Aを元々のIDに戻す
		for(i=0; i<5; i++){
			if(ary[i] == 14) ary[i] = 1;
			if(ary[i] == 28) ary[i] = 15;
			if(ary[i] == 42) ary[i] = 29;
			if(ary[i] == 56) ary[i] = 43;
		}

	for(i=0; i<5; i++){
		if(ary[i] == jid){
			ary[i] = 0;
		}
	}

	return;
	}
}


//--------------------------------------------------------------------------//
// トランプのカードの初期化
//
//
//--------------------------------------------------------------------------//


void SetCard(int card[53]){
	int i,j=0;
	char buf[MAX];
	for(i=0;i<53;i++){
		if(j==14){j++;
		}else if(j==28){j++;
		}else if(j==42){j++;}
		card[i] = j;
		MapCard(card[i],buf);
		printf("Card[%d] %d = %s\n",card[i],j,buf);
		j++;
	}

	return;
}

//--------------------------------------------------------------------------//
// 役の確認
//
//
//--------------------------------------------------------------------------//

int judge_1pair(int *mycards){
	if(((mycards[0] % 14) == (mycards[1] % 14)) ||
	   ((mycards[1] % 14) == (mycards[2] % 14)) ||
   	   ((mycards[2] % 14) == (mycards[3] % 14)) ||
	   ((mycards[3] % 14) == (mycards[4] % 14)))
	return 1;
	else return 0;
}

int judge_2pairs(int *mycards){
	if(((mycards[0] % 14) == (mycards[1] % 14) && (mycards[2] % 14) == (mycards[3] % 14)) ||
	   ((mycards[0] % 14) == (mycards[1] % 14) && (mycards[3] % 14) == (mycards[4] % 14)) ||
	   ((mycards[1] % 14) == (mycards[2] % 14) && (mycards[3] % 14) == (mycards[4] % 14)))
	return 2;
	else return 0;
}

int judge_3cards(int *mycards){
	if(((mycards[0] % 14) == (mycards[1] % 14) && (mycards[0] % 14) == (mycards[2] % 14)) ||
	   ((mycards[1] % 14) == (mycards[2] % 14) && (mycards[1] % 14) == (mycards[3] % 14)) ||
	   ((mycards[2] % 14) == (mycards[3] % 14) && (mycards[2] % 14) == (mycards[4] % 14)))
	return 3;
	else return 0;
}

int judge_straight(int *mycards){
	if((mycards[0] % 14) == ((mycards[1] % 14) + 1) &&
	   (mycards[1] % 14) == ((mycards[2] % 14) + 1) &&
	   (mycards[2] % 14) == ((mycards[3] % 14) + 1) &&
	   (mycards[3] % 14) == ((mycards[4] % 14) + 1))
	return 4;
	else return 0;
}

int judge_flash(int *mycards){
	if((mycards[0] / 14) == (mycards[1] / 14) &&
	   (mycards[0] / 14) == (mycards[2] / 14) &&
	   (mycards[0] / 14) == (mycards[3] / 14) &&
	   (mycards[0] / 14) == (mycards[4] / 14))
	return 5;
	else return 0;
}

int judge_fullHouse(int *mycards){
	if((judge_2pairs(mycards) == 2) && (judge_3cards(mycards) == 3))
	return 6;
	else return 0;
}

int judge_4cards(int *mycards){
	if(((judge_2pairs(mycards) == 2) && (mycards[1] % 14) == (mycards[3] % 14)))
	return 7;
	else return 0;
}

int judge_straightFlash(int *mycards){
	if((judge_straight(mycards) == 4) && (judge_flash(mycards) == 5))
	return 8;
	else return 0;
}

int judge_royalFlash(int *mycards){
	if((judge_straightFlash(mycards) == 8) && (mycards[0] % 14 == 1))
	return 9;
	else return 0;
}

int judge_5cards(int *mycards){
	if((judge_4cards(mycards) == 7) && mycards[4] == 0)
	return 10;
	else return 0;
}

unsigned long CalcScore(int *mycards, int rank){

	int j[5] = {0,0,0,0,0};
	int i;
	unsigned long score = 0;

	SortShow(mycards,5,rank,0);
//	printf("calc:");	// debug
//	for(i=0; i<5; i++) {printf("%d ",mycards[i]);}	// debug

	// ソート済み
	switch(rank){
		case 0: for(i=0; i<5; i++) {j[i] = (mycards[i] % 14);}
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;} // "A"のスコアを14ポイントに変換
			 score = j[0]*S0 + j[1]*S1 + j[2]*S2 + j[3]*S3 + j[4]*S4; break;

		case 1: if((mycards[0] % 14) == (mycards[1] % 14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[2] % 14); j[2] = (mycards[3] % 14); j[3] = (mycards[4] % 14);}
			 if((mycards[1] % 14) == (mycards[2] % 14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[3] % 14); j[3] = (mycards[4] % 14);}
			 if((mycards[2] % 14) == (mycards[3] % 14)) {j[0] = (mycards[2] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[1] % 14); j[3] = (mycards[4] % 14);}
			 if((mycards[3] % 14) == (mycards[4] % 14)) {j[0] = (mycards[3] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[1] % 14); j[3] = (mycards[2] % 14);}
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 1*BASE + j[0]*S0 + j[1]*S1 + j[2]*S2 + j[3]*S3; break;

		case 2: if((mycards[0]%14) == (mycards[1] % 14) && (mycards[2] % 14) == (mycards[3] % 14)) 
			 	{j[0] = (mycards[0] % 14); j[1] = (mycards[2] % 14); j[2] = (mycards[4] % 14);}
			 if((mycards[0]%14) == (mycards[1] % 14) && (mycards[3] % 14) == (mycards[4] % 14)) 
				{j[0] = (mycards[0] % 14); j[1] = (mycards[3] % 14); j[2] = (mycards[2] % 14);}
			 if((mycards[1]%14) == (mycards[2] % 14) && (mycards[3] % 14) == (mycards[4] % 14)) 
				{j[0] = (mycards[1] % 14); j[1] = (mycards[3] % 14); j[2] = (mycards[0] % 14);}
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 2*BASE + j[0]*S0 + j[1]*S1 + j[2]*S2; break;

		case 3: if((mycards[0]%14) == (mycards[2]%14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[3] % 14); j[2] = (mycards[4] % 14);}
			 if((mycards[1]%14) == (mycards[3]%14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[4] % 14);}
			 if((mycards[2]%14) == (mycards[4]%14)) {j[0] = (mycards[2] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[1] % 14);}
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 3*BASE + j[0]*S0 + j[1]*S1 + j[2]*S2; break;

		case 4: j[0] = (mycards[0] % 14);
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 4*BASE + j[0]*S0; break;

		case 5: j[0] = (mycards[0] % 14);
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 5*BASE + j[0]*S0; break;

		case 6: if((mycards[0]%14) == (mycards[2]%14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[3] % 14);}
			 if((mycards[1]%14) == (mycards[3]%14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14);}
			 if((mycards[2]%14) == (mycards[4]%14)) {j[0] = (mycards[2] % 14); j[1] = (mycards[0] % 14);}
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 6*BASE + j[0]*S0 + j[1]*S1; break;

		case 7: if((mycards[0]%14) == (mycards[3]%14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[4] % 14);}
			 if((mycards[1]%14) == (mycards[4]%14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14);}
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 7*BASE + j[0]*S0 + j[1]*S1; break;

		case 8: j[0] = (mycards[0] % 14);
			 for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 8*BASE + j[0]*S0; break;
		
		case 9: for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			 score = 9*BASE; break;

		case 10: j[0] = (mycards[0] % 14);
			  for(i=0; i<5; i++){if(j[i] == 1) j[i] = 14;}
			  score = 10*BASE + j[0]*S0; break;

		default: score = 777;
	}
//	for(i=0; i<5; i++) {printf("j[%d]=%d ",i,j[i]);}	// debug
//	printf("score = %ld\n",score);	// debug
	return score;
}

/*
long calc_score(int *mycards, int rank){

	int j[5] = {0,0,0,0,0};
	int i;
	ulong score;

	for(i=0;i<5;i++){
		printf("ScoreCard %d\n",mycards[i]);
	}

	// ソート済み
	switch(rank){
		case 0: for(i=0; i<5; i++) {j[i] = (mycards[i] % 14);}
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = j[0]*S0 + j[1]*S1 + j[2]*S2 + j[3]*S3 + j[4]*S4; break;
			

		case 1: if((mycards[0] % 14) == (mycards[1] % 14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[2] % 14); j[2] = (mycards[3] % 14); j[3] = (mycards[4] % 14);}
			 if((mycards[1] % 14) == (mycards[2] % 14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[3] % 14); j[3] = (mycards[4] % 14);}
			 if((mycards[2] % 14) == (mycards[3] % 14)) {j[0] = (mycards[2] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[1] % 14); j[3] = (mycards[4] % 14);}
			 if((mycards[3] % 14) == (mycards[4] % 14)) {j[0] = (mycards[3] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[2] % 14); j[3] = (mycards[3] % 14);}
			 for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 1*BASE + j[0]*S0 + j[1]*S1 + j[2]*S2 + j[3]*S3; break;

		case 2: if((mycards[0]%14) == (mycards[1] % 14) && (mycards[2] % 14) == (mycards[3] % 14)) 
			 	{j[0] = (mycards[0] % 14); j[1] = (mycards[2] % 14); j[2] = (mycards[4] % 14);}
			 if((mycards[0]%14) == (mycards[1] % 14) && (mycards[3] % 14) == (mycards[4] % 14)) 
				{j[0] = (mycards[0] % 14); j[1] = (mycards[3] % 14); j[2] = (mycards[2] % 14);}
			 if((mycards[1]%14) == (mycards[2] % 14) && (mycards[3] % 14) == (mycards[4] % 14)) 
				{j[0] = (mycards[1] % 14); j[1] = (mycards[3] % 14); j[2] = (mycards[0] % 14);}
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 2*BASE + j[0]*S0 + j[1]*S1 + j[2]*S2; break;

		case 3: if((mycards[0]%14) == (mycards[2]%14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[3] % 14); j[2] = (mycards[4] % 14);}
			 if((mycards[1]%14) == (mycards[3]%14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[4] % 14);}
			 if((mycards[2]%14) == (mycards[4]%14)) {j[0] = (mycards[2] % 14); j[1] = (mycards[0] % 14); j[2] = (mycards[1] % 14);}
			 for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 3*BASE + j[0]*S0 + j[1]*S1 + j[2]*S2; break;

		case 4: j[0] = (mycards[0] % 14);
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 4*BASE + mycards[0]*S0; break;

		case 5: j[0] = (mycards[0] % 14);
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 5*BASE + mycards[0]*S0; break;

		case 6: if((mycards[0]%14) == (mycards[2]%14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[3] % 14);}
			 if((mycards[1]%14) == (mycards[3]%14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14);}
			 if((mycards[2]%14) == (mycards[4]%14)) {j[0] = (mycards[2] % 14); j[1] = (mycards[0] % 14);}
			 for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 6*BASE + j[0]*S0 + j[1]*S1; break;

		case 7: if((mycards[0]%14) == (mycards[3]%14)) {j[0] = (mycards[0] % 14); j[1] = (mycards[4] % 14);}
			 if((mycards[1]%14) == (mycards[4]%14)) {j[0] = (mycards[1] % 14); j[1] = (mycards[0] % 14);}
			 for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 7*BASE + j[0]*S0 + j[1]*S1; break;

		case 8: j[0] = (mycards[0] % 14);
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			 score = 8*BASE + mycards[0]*S0; break;
		
		case 9: 
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			score = 9*BASE; break;

		case 10: j[0] = (mycards[0] % 14);
			for(i=0;i<5;i++){if(j[i]=mycards[i] % 14);}
			  score = 10*BASE; break;

		default: score = 777;
	}
	for(i=0; i<5; i++) {printf("j[%d] = %d\n",i,j[i]);}
	printf("score = %ld\n",score);
	return score;
}*/

int CheckRank(int *mycards ,char send_buf[MAX]){
	struct maximum{
		int rank;	// 役ランク
		unsigned long score;	// 役スコア
	} max[55] = {};

	int rank = 0;
	int i = 0;
	int j = 0;
	int result[11];
	int maxrank = 0;
	unsigned long maxscore = 0;
	int mycards_backup[4];
	int jid = 0;

	SortNum(mycards,5);
	
	/* 手札にジョーカーあり */
	/* この後ジョーカーに相当する5枚目を全パターン試す度にソートするので他の4枚のバックアップが必要*/
	for(i=0; i<4; i++){
		mycards_backup[i] = mycards[i];
	}

	if(mycards[4] == 0){
		for(i=0; i<56; i++){

			for(j=0; j<4; j++){
				mycards[j] = mycards_backup[j];	// バックアップ戻し
			}

			mycards[4] = i;
			if(mycards[4] != 14
			&& mycards[4] != 28 
			&& mycards[4] != 42 
			&& mycards[4] != mycards_backup[0]
			&& mycards[4] != mycards_backup[1]
			&& mycards[4] != mycards_backup[2]
			&& mycards[4] != mycards_backup[3]){
	
/*
// debug
				printf("ソート前： ");	
				for(j=0; j<5; j++) {printf("%d ",mycards[j]);}
				printf("\n");		
				SortNum(mycards,5);	// 判定用ソート
				printf("ソート後： ");	
				for(j=0; j<5; j++) {printf("%d ",mycards[j]);}
				printf("\n");
*/			
				SortNum(mycards,5);	// 判定用ソート
				result[0] = 0;
				result[1] = judge_1pair(mycards);
				result[2] = judge_2pairs(mycards);
				result[3] = judge_3cards(mycards);
				result[4] = judge_straight(mycards);
				result[5] = judge_flash(mycards);
				result[6] = judge_fullHouse(mycards);
				result[7] = judge_4cards(mycards);
				result[8] = judge_straightFlash(mycards);
				result[9] = judge_royalFlash(mycards);
				result[10] = judge_5cards(mycards);
				
				/* 役ランクとスコア保存 */
				max[i].rank = result[0];
				for(j=1; j<11; j++){
					if(max[i].rank < result[j]){
						max[i].rank = result[j];
					}
				}
				max[i].score = CalcScore(mycards,max[i].rank);
//				printf("max[%d].rank = %d, ",i,max[i].rank);	// debag
//				printf("max[%d].score = %ld,\n",i,max[i].score);	// debug
			}			
		}

		/* 最大スコアを探査 */
		/* 最大ランクおよび、同ランクの内から */
		for(i=0; i<56; i++){
			if(maxrank <= max[i].rank){
				maxrank = max[i].rank;
//				printf("[%d]のとき maxrank = %d\n",i,maxrank);	// debug
				if(maxscore < max[i].score){
					maxscore = max[i].score;
					jid = i;
					printf("jid:%d\n",jid);
//					printf("max[%d].rank = %d, ",i,max[i].rank);	// debug
//					printf("最大スコアが更新されました：%ld\n",maxscore);	// debug
				}
			}
		}
		
		/* 手札を引いた状態に戻し、ジョーカーは最高スコアを出した札にする */
		for(j=0; j<4; j++){
			mycards[j] = mycards_backup[j];	// バックアップ戻し
		}
		mycards[4] = jid;
		rank = maxrank;
		SortShow(mycards,5,rank,jid);
	}	
	
	/* 手札にジョーカーなし */
	else {
		result[0] = 0;
		result[1] = judge_1pair(mycards);
		result[2] = judge_2pairs(mycards);
		result[3] = judge_3cards(mycards);
		result[4] = judge_straight(mycards);
		result[5] = judge_flash(mycards);
		result[6] = judge_fullHouse(mycards);
		result[7] = judge_4cards(mycards);
		result[8] = judge_straightFlash(mycards);
		result[9] = judge_royalFlash(mycards);
	
		rank = result[1];
		for(i=2; i<10; i++){
			if(rank < result[i]){
				rank = result[i];
			}		
		}
		maxscore = CalcScore(mycards,rank);
	}

	/* send_bufに役名を格納　*/
	switch(rank){
		case 0: sprintf(send_buf,"              \033[30mNo Pair\033[39m\033[49m\n"); break;
		case 1: sprintf(send_buf,"              \033[31m1 Pair\033[39m\033[49m\n"); break;
		case 2: sprintf(send_buf,"              \033[31m2 Pairs\033[39m\n"); break;
		case 3: sprintf(send_buf,"              \033[31m3 Cards\033[39m\n"); break;
		case 4: sprintf(send_buf,"              \033[31mStraight\033[39m\n"); break;
		case 5: sprintf(send_buf,"              \033[31mFlash\033[39m\n"); break;
		case 6: sprintf(send_buf,"              \033[31mFullHouse\033[39m\n"); break;
		case 7: sprintf(send_buf,"              \033[31m4 Cards\033[39m\n"); break;
		case 8: sprintf(send_buf,"              \033[31mStraightFlash\033[39m\n"); break;
		case 9: sprintf(send_buf,"              \033[31mRoyalFlash\033[39m\n"); break;
		case 10: sprintf(send_buf,"             \033[31m5 Cards\033[39m\n"); break;		
		default: sprintf(send_buf,"             illegal rank\033[39m\n"); break;
	}

	printf("rank = %d\n",rank);
	printf("score = %ld\n",maxscore);
	//return rank;
	return maxscore;


}
// card[]:hairetu, send_buf[]:message
/*
int CheckRank(int roomid,int memberid,int *mycards ,char send_buf[MAX]){
	int rank = 0;
	int i = 0;
	int j = 0;
	int result[11];
	int in_joker_rank = 0;
	int alternation = 0;
	int mycards_backup[4];

	// 手札にジョーカーあり 
	// この後ジョーカーに相当する5枚目を全パターン試す度にソートするので他の4枚のバックアップが必要
	for(i=0; i<4; i++){
		mycards_backup[i] = mycards[i];
	}

	if(mycards[4] == 0){
		for(i=0; i<56; i++){
	
			printf("%d\n",in_joker_rank);

			for(j=0; j<4; j++){
				mycards[j] = mycards_backup[j];
			}
			mycards[4] = i;
			if(mycards[4] != 14
			&& mycards[4] != 28 
			&& mycards[4] != 42 
			&& mycards[4] != mycards_backup[0]
			&& mycards[4] != mycards_backup[1]
			&& mycards[4] != mycards_backup[2]
			&& mycards[4] != mycards_backup[3]){
				SortNum(mycards,5);
//				for(j=0; j<5; j++) {printf("%d ",mycards[j]);}
//				printf("\n");
				result[0] = 0;
				result[1] = judge_1pair(mycards);
				result[2] = judge_2pairs(mycards);
				result[3] = judge_3cards(mycards);
				result[4] = judge_straight(mycards);
				result[5] = judge_flash(mycards);
				result[6] = judge_fullHouse(mycards);
				result[7] = judge_4cards(mycards);
				result[8] = judge_straightFlash(mycards);
				result[9] = judge_royalFlash(mycards);
				result[10] = judge_5cards(mycards);

				for(j=1; j<11; j++){
					if(in_joker_rank < result[j]){
					in_joker_rank = result[j];
					alternation = i;
					}
				}		
			}
			
			for(j=0;j<4;j++){
				mycards[j] = mycards_backup[j];

			}
			mycards[4]=0;

			rank = in_joker_rank;
			printf("points: %ld",calc_score(mycards,rank));
		}				
		/* 最も強い役になる位置にジョーカーを配置 
		
		rank = in_joker_rank;
		SortNum(mycards,5);
		for(j=0; j<5; j++){
			if(mycards[j] == alternation)
				mycards[j] = 0;
		for(j=0; j<5; j++) {printf("%d ",mycards[j]);}
		}
		
		
	}	
	
	// 手札にジョーカーなし 
	else {
		result[0] = 0;
		result[1] = judge_1pair(mycards);
		result[2] = judge_2pairs(mycards);
		result[3] = judge_3cards(mycards);
		result[4] = judge_straight(mycards);
		result[5] = judge_flash(mycards);
		result[6] = judge_fullHouse(mycards);
		result[7] = judge_4cards(mycards);
		result[8] = judge_straightFlash(mycards);
		result[9] = judge_royalFlash(mycards);
	
		rank = result[1];
		for(i=2; i<10; i++){
			if(rank < result[i]){
				rank = result[i];
			}		
		}
	}

	printf("\nrank = %d\n",rank);
	points = calc_score(mycards,rank);	// scoreを画面出力もする

	// send_bufに役名を格納　
	switch(rank){
		case 0: sprintf(send_buf,"              \033[30mNo Pair\033[39m\033[49m\n"); break;
		case 1: sprintf(send_buf,"              \033[31m1 Pair\033[39m\033[49m\n"); break;
		case 2: sprintf(send_buf,"              \033[31m2 Pairs\033[39m\n"); break;
		case 3: sprintf(send_buf,"              \033[31m3 Cards\033[39m\n"); break;
		case 4: sprintf(send_buf,"              \033[31mStraight\033[39m\n"); break;
		case 5: sprintf(send_buf,"              \033[31mFlash\033[39m\n"); break;
		case 6: sprintf(send_buf,"              \033[31mFullHouse\033[39m\n"); break;
		case 7: sprintf(send_buf,"              \033[31m4 Cards\033[39m\n"); break;
		case 8: sprintf(send_buf,"              \033[31mStraightFlash\033[39m\n"); break;
		case 9: sprintf(send_buf,"              \033[31mRoyalFlash\033[39m\n"); break;
		case 10: sprintf(send_buf,"             \033[31m5 Cards\033[39m\n"); break;		
		default: sprintf(send_buf,"             illegal rank\033[39m\n"); break;
	}
	data[roomid].rankvalue[memberid] = points;
	printf("Player%d RANKVALUE = %ld",memberid,points);
	return rank;


}
*/

//--------------------------------------------------------------------------//
// カードを引く
//
//
//--------------------------------------------------------------------------//

int DrawCard(int card[],int deck[],int num,int pointer,int max){
	int i;
	for(i=0;i<num;i++){
		card[i] = deck[pointer];
		pointer++;
		if(pointer==max){pointer = 0;}
		//printf("Draw[%d]",card[i]);
	}
	Sort(card,5);
	return pointer;
}

int DrawOneCard(int card[],int deck[],int card_p,int deck_p,int max){
	int i;

	card[card_p] = deck[deck_p];
	deck_p++;
	if(deck_p==max){deck_p = 0;}
	
	return deck_p;
}

//--------------------------------------------------------------------------//
// カードの交換
//
//
//--------------------------------------------------------------------------//

int ChangeCard(int code,int card[],int deck[],int deck_p,int max){
	//printf("\ncode[[[[%d]]]]\n",code);
	if(code % 2 == 0){
		deck_p=DrawOneCard(card,deck,0,deck_p,53);
	}
	if(code % 3 == 0){
		deck_p=DrawOneCard(card,deck,1,deck_p,53);
	}
	if(code % 5 == 0){
		deck_p=DrawOneCard(card,deck,2,deck_p,53);
	}
	if(code % 7 == 0){
		deck_p=DrawOneCard(card,deck,3,deck_p,53);
	}	
	if(code % 11 == 0){
		deck_p=DrawOneCard(card,deck,4,deck_p,53);
	}

	return deck_p;
}

//--------------------------------------------------------------------------//
// マスターの処理待ち関数
//
//
//--------------------------------------------------------------------------//

void WaitPlayer(int roomid,int state){
	int i;
	for(i=1;i<=data[roomid].participants;i++){
		while(data[roomid].gamestate[i] != state){if(data[roomid].participants==0){Ctrl_c();}}
	}
	return;
}

//--------------------------------------------------------------------------//
// 表示用関数
//
//
//--------------------------------------------------------------------------//
void ShowScore(int roomid){
	int participants = data[roomid].participants;
	char str[MAX];
	int i,j,x,y;

	
	
	for(i=1;i<=participants;i++){
		x = 40;
		y = 1;
		sprintf(str,"\033[%d;%dH┌────────────────────────────────────┐",y,x);
		/*data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0);*/

		for(j=1;j<=participants;j++){
			y = y + 1;
			if(i == j){
				sprintf(str,"%s\033[%d;%dH│\033[1mPlayer%d\033[0m     $%7d                │",str,y,x,j,data[roomid].money[j]);
			}else{
				sprintf(str,"%s\033[%d;%dH│Player%d     $%7d                │",str,y,x,j,data[roomid].money[j]);
			}
			
			//data[roomid].gamestate[i] = 2;
			//while(data[roomid].gamestate[i] != 0);
		}
		y = y+1;

		sprintf(str,"%s\033[%d;%dH│                                    │",str,y,x);
		//data[roomid].gamestate[i] = 2;
		//while(data[roomid].gamestate[i] != 0);
		
		y = y+1;
		sprintf(str,"%s\033[%d;%dH│Total Bet   $%7d                │",str,y,x,data[roomid].fieldmoney);
		//data[roomid].gamestate[i] = 2;
		//while(data[roomid].gamestate[i] != 0);

		y = y+1;
		sprintf(str,"%s\033[%d;%dH└────────────────────────────────────┘",str,y,x);
		//data[roomid].gamestate[i] = 2;
		//while(data[roomid].gamestate[i] != 0);

		y = 19;
		x = 1;
		sprintf(data[roomid].send_buf[i],"%s\033[%d;%dH",str,y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		

	}
	return;
}

void ShowState(int roomid, int memberid){
	int participants = data[roomid].participants;
	char str[MAX];
	int i,j,x,y;

	
	
	for(i=1;i<=participants;i++){
		x = 40;
		y = 13;
		sprintf(str,"\033[%d;%dH┌────────────────────────────────────┐",y,x);
		//data[roomid].gamestate[i] = 2;
		//while(data[roomid].gamestate[i] != 0);

		for(j=1;j<=participants;j++){
			x = 40;
			y = y + 1;
			sprintf(str,"%s\033[%d;%dH│  Player%d",str,y,x,j);
			//data[roomid].gamestate[i] = 2;
			//while(data[roomid].gamestate[i] != 0);
			x = x + 37;
			sprintf(str,"%s\033[%d;%dH│",str,y,x);
			//data[roomid].gamestate[i] = 2;
			//while(data[roomid].gamestate[i] != 0);
			
		}
		x = 40;
		y = y+1;
		sprintf(str,"%s\033[%d;%dH└────────────────────────────────────┘",str,y,x);
		//data[roomid].gamestate[i] = 2;
		//while(data[roomid].gamestate[i] != 0);

		y = 13;
		x = 41;
		for(j=1;j<=participants;j++){
			y++;
			if(j == memberid){
				sprintf(str,"%s\033[%d;%dH\033[34m\033[1m→\033[39m\033[0m",str,y,x);
			}else{
				sprintf(str,"%s\033[%d;%dH ",str,y,x);
			}
			//data[roomid].gamestate[i] = 2;
			//while(data[roomid].gamestate[i] != 0);
		}
		y = 19;
		x = 1;
		sprintf(data[roomid].send_buf[i],"%s\033[%d;%dH",str,y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		

	}
	return;
}

void ShowAction(int roomid, int memberid){
	int participants = data[roomid].participants;
	char str[MAX];
	int i,j,x,y;

	
	
	for(i=1;i<=participants;i++){
		y = 13;
		x = 54;
		y = y + memberid;

		sprintf(data[roomid].send_buf[i],"\033[%d;%dH",y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH%s  ",y,x,data[roomid].action[memberid]);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		y = 19;
		x = 1;
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH",y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		

	}
	return;
}

void ShowChange(int roomid){
	int participants = data[roomid].participants;
	char str[MAX];
	int i,j,x,y;

	for(i=1;i<=participants;i++){
		y = 14;
		x = 62;

		sprintf(str,"");
		for(j=1;j<=participants;j++){
			sprintf(str,"%s\033[%d;%dH%d枚交換",str,y,x,data[roomid].changenum[j]);
			y++;
		}

		sprintf(data[roomid].send_buf[i],"%s",str);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		y = 19;
		x = 1;
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH",y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		

	}
	return;
}

void ShowMessage(int roomid, char str[MAX],int position){
	int participants = data[roomid].participants;
	int i,j,x,y;

	
	
	for(i=1;i<=participants;i++){
		y = 20;
		x = 1;
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH┌───────────────────────────────────────────────────────────────────────────┐",y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};
		
		if(position == 1){
			y++;
			sprintf(data[roomid].send_buf[i],"\033[%d;%dH│                                                                           │",y,x);
			data[roomid].gamestate[i] = 2;
			while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

			x++;
			sprintf(data[roomid].send_buf[i],"\033[%d;%dH%s",y,x,str);
			data[roomid].gamestate[i] = 2;
			while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

			x--;
			y++;
			sprintf(data[roomid].send_buf[i],"\033[%d;%dH│                                                                           │",y,x);
			data[roomid].gamestate[i] = 2;
			while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};
		}else if(position == 2){
			y += 2;
			sprintf(data[roomid].send_buf[i],"\033[%d;%dH│                                                                           │",y,x);
			data[roomid].gamestate[i] = 2;
			while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

			x++;
			sprintf(data[roomid].send_buf[i],"\033[%d;%dH%s",y,x,str);
			data[roomid].gamestate[i] = 2;
			while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};
			x--;

		}
		y++;
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH└───────────────────────────────────────────────────────────────────────────┘",y,x);
		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};

		y = 19;
		x = 1;
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH",y,x);

		data[roomid].gamestate[i] = 2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}};
		

	}
	return;
}

//--------------------------------------------------------------------------//
// BET処理関数
//
//
//--------------------------------------------------------------------------//

void RaiseMaster(int roomid, int raiser){
	int i,j,next;
	int participants = data[roomid].participants;
	char str[MAX];

	
	data[roomid].betmoney = data[roomid].betmoney + 100;
	data[roomid].money[raiser] -= data[roomid].betmoney;
	data[roomid].fieldmoney += data[roomid].betmoney;
	ShowScore(roomid);
	ShowAction(roomid,raiser);

	//for(j=1;j<=participants;j++){
		sprintf(str,"Player%dが%sしました",raiser,data[roomid].action[raiser]);
		ShowMessage(roomid,str,1);
	//}

	next = raiser + 1;

	for(i=next;i<=participants;i++){
		ShowState(roomid,i);
		sprintf(str,"Player%dが考え中です...",i);
		ShowMessage(roomid,str,2);
		if(data[roomid].falled[i] == 1){
			printf("降りている\n");
			continue;
		}

		data[roomid].gamestate[i] = 7;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}

		switch(data[roomid].actionint[i]){
			case 3:	//call
				data[roomid].money[i]-= data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 4:
				RaiseMaster(roomid,i);
				return;
				break;
			case 5:	//falled
				data[roomid].falled[i] = 1;
				data[roomid].falledcount++;
				printf("falledしたa\n");
				break;
		}

		sprintf(str,"Player%dが%sしました",i,data[roomid].action[i]);
		ShowMessage(roomid,str,1);
	

	}

	ShowScore(roomid);

	for(i=1;i<raiser;i++){
		ShowState(roomid,i);
		sprintf(str,"Player%dが考え中です...",i);
		ShowMessage(roomid,str,2);
		if(data[roomid].falled[i] == 1){
			printf("降りている\n");
			continue;
		}

		data[roomid].gamestate[i] = 7;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}

		switch(data[roomid].actionint[i]){
			case 3:	//call
				data[roomid].money[i]-= data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 4:
				RaiseMaster(roomid,i);
				return;
				break;
			case 5:	//falled
				data[roomid].falled[i] = 1;
				data[roomid].falledcount++;
				break;
		}
		ShowAction(roomid,i);
		ShowScore(roomid);
		//for(j=1;j<=participants;j++){
			sprintf(str,"Player%dが%sしました",i,data[roomid].action[i]);
			ShowMessage(roomid,str,1);
		//}

	}
	return;
	

}


void BetMaster(int roomid,int parentid){
	int i,j,k;
	int raiseflag = 0;
	int participants = data[roomid].participants;
	int secondflag = 0;
	char str[MAX];
	int winner;

	data[roomid].betmoney = 100;

	if(data[roomid].participants == 1){
		return;
	}

	if(data[roomid].beted == 1){
		secondflag = 1;
		data[roomid].beted = 0;
	}

	i = parentid;
	while(1){
		ShowState(roomid,i);
		ShowScore(roomid);

		sprintf(str,"Player%dが考え中です...",i);
		ShowMessage(roomid,str,2);

		if(data[roomid].falled[i] == 1){
			i++;
			if(i > participants){
				i = 1;
			}

			if(i == parentid){
				break;
			}
			printf("降りている\n");

			continue;
		}

		if(secondflag == 0){
			data[roomid].gamestate[i]=6;
		}else{
			data[roomid].gamestate[i]=8;
		}

		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}

		switch(data[roomid].actionint[i]){
			case 1://bet
				data[roomid].beted = 1;
				data[roomid].money[i]-=data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 2:	//check
				break;
			case 3:	//call
				data[roomid].money[i]-= data[roomid].betmoney;
				data[roomid].fieldmoney += data[roomid].betmoney;
				break;
			case 4:
				RaiseMaster(roomid,i);
				raiseflag = 1;
				break;
			case 5:	//falled
				data[roomid].falled[i] = 1;
				data[roomid].falledcount++;
				break;
		}
		if(raiseflag == 0){
			//for(j=1;j<=participants;j++){
				sprintf(str,"Player%dが%sしました",i,data[roomid].action[i]);
				ShowMessage(roomid,str,1);
			//}
		}
		ShowAction(roomid,i);
		ShowScore(roomid);
		if(participants - data[roomid].falledcount == 1){
			for(j=1;j<=participants;j++){
				if(data[roomid].falled[j] == 0){
					winner = j;
					//チップの支払い
					data[roomid].money[j] += data[roomid].fieldmoney;
				}
			}	
			sprintf(str,"勝負できるプレイヤーがいなくなりました。Player%dの勝利です。",winner);
			ShowMessage(roomid,str,1);
			
			for(i=1;i<=participants;i++){
				data[roomid].gamestate[i] = 10;
			}
			WaitPlayer(roomid,0);
			break;
		}

		i++;

		if(i > participants){
			i = 1;
		}

		if(i == parentid){
			break;
		}
	}
	return;
}

void RaisePlayer(int roomid,int memberid,char *recv_buf){
	while(1){
		SendMes(roomid,"\033[16;1H                     ","RECV","PRINT");
		SendMes(roomid,"\033[16;1HRaise>","SEND","PRINT");
		RecvMes(roomid,recv_buf);

		if(data[roomid].participants == 0){
			return;
		}

		sscanf(recv_buf,"%[^,]",data[roomid].action[memberid]);
		if(strcmp(data[roomid].action[memberid],"bet")==0){
			SendMes(roomid,"できないよ","RECV","PRINT");
			continue;
		}else if(strcmp(data[roomid].action[memberid],"check")==0){
			SendMes(roomid,"できないよ","RECV","PRINT");
			continue;
		}else if(strcmp(data[roomid].action[memberid],"call")==0){
			data[roomid].actionint[memberid] = 3;
			break;
		}else if(strcmp(data[roomid].action[memberid],"raise")==0){
			data[roomid].actionint[memberid] = 4;
			break;
		}else if(strcmp(data[roomid].action[memberid],"falled")==0){
			data[roomid].actionint[memberid] = 5;
			break;
		}else{
			data[roomid].actionint[memberid] = 0;
			SendMes(roomid,"できんがな","RECV","PRINT");
		}
	}
	return;
}

void BetPlayer(int roomid,int memberid,char *recv_buf){
	char str[MAX];
	while(1){
		SendMes(roomid,"\033[16;1H                     ","RECV","PRINT");
		SendMes(roomid,"\033[16;1HBET>","SEND","PRINT");
		RecvMes(roomid,recv_buf);

		if(data[roomid].participants == 0){
			return;
		}

		sscanf(recv_buf,"%[^,]",data[roomid].action[memberid]);
		if(strcmp(data[roomid].action[memberid],"bet")==0){
			data[roomid].actionint[memberid] = 1;
		}else if(strcmp(data[roomid].action[memberid],"check")==0){
			data[roomid].actionint[memberid] = 2;
		}else if(strcmp(data[roomid].action[memberid],"call")==0){
			data[roomid].actionint[memberid] = 3;
		}else if(strcmp(data[roomid].action[memberid],"raise")==0){
			data[roomid].actionint[memberid] = 4;
		}else if(strcmp(data[roomid].action[memberid],"falled")==0){
			data[roomid].actionint[memberid] = 5;
		}else{
			data[roomid].actionint[memberid] = 0;
		}
		if(data[roomid].beted == 0){
			if(data[roomid].actionint[memberid]==1 || data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes(roomid,"できません","RECV","PRINT");
			}
		}else{
			if(data[roomid].actionint[memberid]==3 || data[roomid].actionint[memberid]==4|| data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes(roomid,"できません","RECV","PRINT");
			}
		}
	}
	return;
}

void BetPlayer2(int roomid,int memberid,char *recv_buf){
	char str[MAX];
	while(1){
		SendMes(roomid,"\033[16;1H                     ","RECV","PRINT");
		SendMes(roomid,"\033[16;1HBET2>","SEND","PRINT");
		RecvMes(roomid,recv_buf);

		if(data[roomid].participants == 0){
			return;
		}

		sscanf(recv_buf,"%[^,]",data[roomid].action[memberid]);
		if(strcmp(data[roomid].action[memberid],"bet")==0){
			data[roomid].actionint[memberid] = 1;
		}else if(strcmp(data[roomid].action[memberid],"check")==0){
			data[roomid].actionint[memberid] = 2;
		}else if(strcmp(data[roomid].action[memberid],"call")==0){
			data[roomid].actionint[memberid] = 3;
		}else if(strcmp(data[roomid].action[memberid],"raise")==0){
			data[roomid].actionint[memberid] = 4;
		}else if(strcmp(data[roomid].action[memberid],"falled")==0){
			data[roomid].actionint[memberid] = 5;
		}else{
			data[roomid].actionint[memberid] = 0;
		}
		if(data[roomid].beted==0){
			if(data[roomid].actionint[memberid]==1 || data[roomid].actionint[memberid]==2 || data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes(roomid,"できません\n","RECV","PRINT");
			}
		}else{
			if(data[roomid].actionint[memberid]==3 || data[roomid].actionint[memberid]==4|| data[roomid].actionint[memberid]==5){
				break;
			}else{
				SendMes(roomid,"できません\n","RECV","PRINT");
			}
		}
	}
	return;
}

void CheckWin(int roomid){
	int i,j;
	int participants = data[roomid].participants;
	int winner = 1;
	char str[MAX];

	if(data[roomid].participants == 1){
		data[roomid].money[1] += data[roomid].fieldmoney;
		data[roomid].fieldmoney = 0;
		for(i=1;i<=participants;i++){
			data[roomid].gamestate[i] = 9;
		}

		WaitPlayer(roomid,0);
		return;
	}

	for(i=1;i<participants;i++){
		if(data[roomid].rankvalue[i] > data[roomid].rankvalue[i+1]){
			winner = i;
		}else{
			winner = i + 1;
		}
	}
	data[roomid].money[winner] += data[roomid].fieldmoney;
	
	ShowScore(roomid);
	
	for(i=1;i<=participants;i++){
		if(winner == i){
			sprintf(data[roomid].send_buf[i],"\033[23;1Hあなたの勝利です。$%dが支払われます。",data[roomid].fieldmoney);
		}else{
			sprintf(data[roomid].send_buf[i],"\033[23;1HPlayer%dの勝利です。$%dが支払われます。",winner,data[roomid].fieldmoney);
		}
		data[roomid].gamestate[i]=2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}
	}
	data[roomid].fieldmoney = 0;
	
	for(i=1;i<=participants;i++){
		data[roomid].gamestate[i] = 9;
	}

	WaitPlayer(roomid,0);


	return;
}

void DealCard(int roomid){
	int i;
	int participants = data[roomid].participants;
	char str[MAX];
	//if(data[roomid].participants != 1){
	
		for(i=1;i<=participants;i++){
			sprintf(data[roomid].send_buf[i],"\033[2J\e[0;0H第%dゲームスタート\n",data[roomid].gamecount);
			data[roomid].fieldmoney += 100;
			data[roomid].money[i] -= 100;
			data[roomid].gamestate[i]=2;
			while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}
		}

	//}

	for(i=1;i<=participants;i++){
		data[roomid].deck_p=DrawCard(data[roomid].mycards[i],data[roomid].deck,5,data[roomid].deck_p,CARDMAX);
		SortNum(data[roomid].mycards[i],5);
		data[roomid].rankvalue[i] = CheckRank(data[roomid].mycards[i],data[roomid].rank[i]);
		MapGraph(5,data[roomid].mycards[i],data[roomid].send_buf[i]);
		sprintf(str,"%s",data[roomid].rank[i]);
		sprintf(data[roomid].send_buf[i],"%s%s",data[roomid].send_buf[i],str);
		data[roomid].gamestate[i]=2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}

	}
	ShowMessage(roomid,"参加金$100を支払います。",1);
	return;
}

void ChangeMaster(int roomid){
	int i;
	int participants = data[roomid].participants;
	char str[MAX];

	for(i=1;i<=participants;i++){
		if(data[roomid].falled[i] == 0){
			data[roomid].gamestate[i] = 4;
		}
	}

	WaitPlayer(roomid,0);

	for(i=1;i<=participants;i++){
		sprintf(data[roomid].send_buf[i],"\033[%d;%dH",10,1);
		data[roomid].gamestate[i]=2;
		while(data[roomid].gamestate[i] != 0){if(data[roomid].participants==0){Ctrl_c();}}
	}

	for(i=1;i<=participants;i++){
		if(data[roomid].falled[i] == 0){				
			data[roomid].deck_p=ChangeCard(data[roomid].changecode[i],data[roomid].mycards[i],data[roomid].deck,data[roomid].deck_p,53);
			SortNum(data[roomid].mycards[i],5);
			data[roomid].rankvalue[i] = CheckRank(data[roomid].mycards[i],data[roomid].rank[i]);
			MapGraph(5,data[roomid].mycards[i],data[roomid].send_buf[i]);
			sprintf(str,"%s",data[roomid].rank[i]);
			sprintf(data[roomid].send_buf[i],"%s%s",data[roomid].send_buf[i],str);
			data[roomid].gamestate[i]=2;
		}
	}
	WaitPlayer(roomid,0);

	return;
}

void OpenCard(int roomid){
	int i;
	int participants = data[roomid].participants;
	if(data[roomid].participants == 1){
		return;
	}
	for(i=1;i<=participants;i++){
		//if(data[roomid].falled[i] == 0){
			data[roomid].gamestate[i]=5;
		//}
	}
	WaitPlayer(roomid,0);
	
	return;

}

void InitGame(int roomid){
	int i;
	int participants = data[roomid].participants;
	data[roomid].beted = 0;
	data[roomid].fieldmoney = 0;
	data[roomid].falledcount = 0;
	data[roomid].betmoney = 100;
	for(i=1;i<=participants;i++){
		data[roomid].falled[i] = 0;
		
	}
	
	return;
}



//--------------------------------------------------------------------------//
// ポーカーゲームマスターの動作
//
//
//--------------------------------------------------------------------------//

void PokerMaster(int roomid){
	int i;
	int deck_p=0;
	int parentid = 1;



	while(data[roomid].participants<roomnum[roomid]){}				//メンバーが揃うまで待機1

	SetCard(data[roomid].deck);

	while(1){
		InitGame(roomid);
		Shuffle(data[roomid].deck,CARDMAX);
		
		//カードを配る
		DealCard(roomid);

		ShowScore(roomid);
		
		

		//第1BET

		BetMaster(roomid,parentid);
		if(data[roomid].participants - data[roomid].falledcount == 1 && data[roomid].participants != 1){
			data[roomid].gamecount++;
			parentid++;
			if(parentid > data[roomid].participants){
				parentid = 1;
			}
			continue;
		}
		ShowScore(roomid);
		//カード交換
		ChangeMaster(roomid);
		ShowScore(roomid);
		ShowChange(roomid);
		//第2BET
		BetMaster(roomid,parentid);
		if(data[roomid].participants - data[roomid].falledcount == 1 && data[roomid].participants != 1){
			data[roomid].gamecount++;
			parentid++;
			if(parentid > data[roomid].participants){
				parentid = 1;
			}
			continue;
		}
		ShowScore(roomid);
		//カード公開
		OpenCard(roomid);
		
		CheckWin(roomid);
		
		parentid++;
		if(parentid > data[roomid].participants){
			parentid = 1;
		}

		data[roomid].gamecount++;
	}

	return;
}

void PokerMember(int roomid){
	int i,n;
	int memberid;
	int code;
	char *s;
	char recv_buf[MAX],send_buf[MAX],str[MAX];

	data[roomid].participants++;
	memberid = data[roomid].participants;
	SendMes(roomid,"Poker Start\n","RECV","PRINT");
	sprintf(send_buf,"\033[2J\e[0;0HあなたのIDは%dです。通信待機中\n",memberid);
	SendMes(roomid,send_buf,"RECV","PRINT");
	data[roomid].gamestate[memberid] = 0;
	while(1){
		while(data[roomid].gamestate[memberid] == 0){
			if(data[roomid].participants==0){
				SendMes(roomid,"\033[2J\e[0;0H切断されました。ロビーに戻ります\n","RECV","PRINT");
				//Ctrl_c();	
				return;
			}

			if((unsigned int)time(NULL) % 5 == 0){
				SendMes(roomid,"A","CHECK","A");
				RecvMes(roomid,str);
				sleep(1);
			}
			

		}
		switch(data[roomid].gamestate[memberid]){
			case 1:	
				RecvMes(roomid,data[roomid].recv_buf[memberid]);
				break;
			case 2:
				SendMes(roomid,data[roomid].send_buf[memberid],"RECV","PRINT");
				//RecvMes(data[roomid].recv_buf[memberid]);
				break;
			case 3:
				SendMes(roomid,data[roomid].send_buf[memberid],"SEND","PRINT");
				RecvMes(roomid,data[roomid].recv_buf[memberid]);
				break;
			case 4:
				SendMes(roomid,"\033[16;1H                     ","RECV","PRINT");
				SendMes(roomid,"\033[16;1H交換>","CHANGE","PRINT");
				RecvChange(roomid,recv_buf);
				if(data[roomid].participants == 0){return;}
				code = 1;
				n = 0;
				s = strtok(recv_buf," ");

				while(s != NULL){
					printf("![%s]!",s);
					switch(atoi(s)){
						case 1: code = code * 2; n++;break;
						case 2: code = code * 3; n++;break;
						case 3: code = code * 5; n++;break;
						case 4: code = code * 7; n++;break;
						case 5: code = code * 11; n++;break;
						default : code = code * 1; break;
					}
					s = strtok(NULL," ");
				}
				if(strncmp(recv_buf,"all",3)==0){
					data[roomid].changenum[memberid] = 5;
					data[roomid].changecode[memberid] = 2 * 3 * 5 * 7 * 11;
				}else{
					data[roomid].changenum[memberid] = n;
					data[roomid].changecode[memberid] = code;
				}

				
				sprintf(send_buf,"\033[21;2H通信待機中...                  ");
				SendMes(roomid,send_buf,"RECV","PRINT");
				break;
			case 5:		//手札公開
				sprintf(send_buf,"\033[2J\e[0;0H手札オープン\nYou:\n");
				SendMes(roomid,send_buf,"RECV","PRINT");
				data[roomid].rankvalue[memberid] = CheckRank(data[roomid].mycards[memberid],data[roomid].rank[memberid]);
				MapGraph(5,data[roomid].mycards[memberid],send_buf);
				sprintf(str,"%s",data[roomid].rank[memberid]);
				sprintf(send_buf,"%s%s",send_buf,str);
				SendMes(roomid,send_buf,"RECV","PRINT");

				for(i=1;i<=data[roomid].participants;i++){
					if(i!=memberid){
						sprintf(send_buf,"Player%d:\n",i);
						SendMes(roomid,send_buf,"RECV","PRINT");
						data[roomid].rankvalue[i] = CheckRank(data[roomid].mycards[i],data[roomid].rank[i]);
						MapGraph(5,data[roomid].mycards[i],send_buf);
						sprintf(str,"%s",data[roomid].rank[i]);
						sprintf(send_buf,"%s%s",send_buf,str);
						SendMes(roomid,send_buf,"RECV","PRINT");
					}
				}
				break;
			case 6:
				BetPlayer(roomid,memberid,recv_buf);
				break;
			case 7:
				RaisePlayer(roomid,memberid,recv_buf);
				break;
			case 8:
				BetPlayer2(roomid,memberid,recv_buf);
				break;
			case 9:
				//sprintf(send_buf,"\033[18;1H次のゲームに進む>");
				sprintf(send_buf,"次のゲームに進む>");
				SendMes(roomid,send_buf,"SEND","PRINT");
				RecvMes(roomid,recv_buf);
				if(data[roomid].participants == 0){return;}
				//SendMes("\033[19;2H通信待機中...                  ","RECV","PRINT");
				SendMes(roomid,"通信待機中...                                                           ","RECV","PRINT");
				break;
			case 10:
				sprintf(send_buf,"\033[%d;%dH次のゲームに進む>",CONSOLE_Y,CONSOLE_X);
				SendMes(roomid,send_buf,"SEND","PRINT");
				RecvMes(roomid,recv_buf);
				if(data[roomid].participants == 0){return;}
				sprintf(send_buf,"\033[%d;%dH通信待機中...                                              ",MSGWIN_Y,MSGWIN_X);
				SendMes(roomid,send_buf,"RECV","PRINT");
				break;	
			default:
				break;
		}
		data[roomid].gamestate[memberid] = 0;
	}
}

//--------------------------------------------------------------------------//
// ポーカーゲーム本体
//
//
//--------------------------------------------------------------------------//

void PorkerGame(void){
	int i,j,pid;
	int x,y;
	int deck_num=0;
	int deck[53];
	int mycards[5];
	int myrank;
	int roomid,master=0;
	int memberid;
	char str[MAX];
	char send_buf[MAX];
	char recv_buf[MAX];


	
	while(1){
		//sprintf(send_buf,"\033[2J\e[0;0H部屋を選んでね。\n");
		sprintf(send_buf,"\033[2J\e[0;0H┌───────────────────────────────────────────────────────────────────────────┐\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                            \033[1mNetwork Poker Game\033[0m                             │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "└───────────────────────────────────────────────────────────────────────────┘\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "┌───────────────────────────────────────────────────────────────────────────┐\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "│                                                                           │\n");
		SendMes(roomid,send_buf,"RECV","PRINT");
		sprintf(send_buf,              "└───────────────────────────────────────────────────────────────────────────┘\n");
		SendMes(roomid,send_buf,"RECV","PRINT");

		for(i=1;i<=ROOMMAX;i++){
			sprintf(str,"\033[%d;%dHRoom %2d : %d / %d",5+i,4,i,data[i].participants,roomnum[i]);
			if(data[i].participants == roomnum[i]){
				sprintf(str,"%s \x1b[31m満員\x1b[39m",str);
			}
			sprintf(send_buf,"%s\n",str);
			SendMes(roomid,send_buf,"RECV","PRINT");
		}

		y = 5;
		x = 50;
		sprintf(str,"\033[%d;%dH┌─────┐",y+1,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH│\x1b[31m♥\x1b[39m ┌──┴──┐",y+2,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH│  │♣ ┌──┴──┐",y+3,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH│  │  │\x1b[31m◆\x1b[39m ┌──┴──┐",y+4,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH└──┤  │  │♠ ┌──┴──┐",y+5,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH   └──┤  │  │     │",y+6,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH      └──┤  │Joker│",y+7,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH         └──┤     │",y+8,x);
		SendMes(roomid,str,"RECV","PRINT");
		sprintf(str,"\033[%d;%dH            └─────┘",y+9,x);
		SendMes(roomid,str,"RECV","PRINT");

		sprintf(str,"\033[%d;%dH>",6+ROOMMAX,3);
		SendMes(roomid,str,"SEND","PRINT");
		RecvMes(roomid,recv_buf);
		if(strncmp(recv_buf,"exit",4)==0){
			SendMes(roomid,"\033[2J\e[0;0H切断します\n","QUIT","PRINT");
			Ctrl_c();
		}
		roomid = atoi(recv_buf);

		if(roomid>10 || roomid == 0){
			continue;
		}


		if(data[roomid].participants < roomnum[roomid]){
			break;
		}else{
			sprintf(str,"\033[2J\e[0;0H部屋[%d]は満員です。\n",roomid);
		}
	}

	pid=fork();
	if(pid!=0){
		close(newsock);
		if(data[roomid].master == 0){
			data[roomid].master = 1;
			master = 1;
		}else{
			exit(1);
		}
	}

	if(master==1){
		PokerMaster(roomid);
		Ctrl_c();
	}else{
		PokerMember(roomid);
		return;
	}
	
}

//--------------------------------------------------------------------------//
// メイン関数
//
//
//--------------------------------------------------------------------------//

int main(void){
	//FILE *fp;
	int cn;
	int i;
	int pid;
	
	int gamenum;
	srand((unsigned int)time(NULL));
	
	Init_signal();
	ShmSetup();
	InitShm();
	ServerSetup();

	

	while(1){
		len_c = sizeof(sain_c);
		newsock = accept(sock,(struct sockaddr*)&sain_c,&len_c);
		if(newsock==-1){
			perror("accept");
			exit(1);
		}
		
		//child process generate
		pid=fork();
		if(pid!=0){
			close(newsock);
			continue;
		}
		while(1){
			PorkerGame();
		}
		

		printf("Connection closed.\n");
		close(newsock);
		break;
	}
	return 0;
}


